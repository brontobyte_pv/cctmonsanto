<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open_multipart('cobros/save/'.$info->cobro_id,array('id'=>'cobros_form'));
?>
<fieldset id="item_kit_info">
<legend><?php echo $this->lang->line("cobros_nuevo"); ?></legend>
<?php  echo form_label('Seleccione un cliente antes de hacer un cobro', 'modo',array('class'=>'wide'));?>

</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{
	$('#cobros_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_cobros_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			cantidad:"required"
			
		},
		messages:
		{
			cantidad:"<?php echo $this->lang->line('items_name_required'); ?>"
			
		}
	});
});

</script>
