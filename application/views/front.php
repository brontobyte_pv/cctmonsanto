<!DOCTYPE html>
<html lang="es">

<head> 
<meta charset="UTF-8">
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="index, follow" />
<meta property="og:url"           content="http://brontobytemx.com/" />
    <meta property="og:type"          content="Servicios TI profesionales" />
    <meta property="og:title"         content="Brontobyte Home" />
    <meta property="og:description"   content="Soporte Tecnico Para empresas, Creación de Sitios WEB profesionales, Soluciones, Desarrollo de portales web con bases de datos, páginas institucionales, " />
    <meta property="og:image"         content="http://brontobytemx.com/images/img-header/Logo-head-bontobytes.png" />
</head>
<meta name="author" content="Brontobyte Computaci&oacute;n">

<title>Brontobyte Home</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-contenido.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/blueberry.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/responsive.css" />
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,300,600' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300italic,500' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="images/ico/ico-logo.ico">
<!-- bootstrop-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap.css">

<!-- bootstrop-->
    <style>

    /* CUSTOMIZE THE CAROUSEL
    -------------------------------------------------- */

    /* Carousel base class */
    .carousel {
      margin-bottom: 60px;
    }

    .carousel .container {
      position: relative;
      z-index: 9;
    }

    .carousel-control {
      height: 80px;
      margin-top: 0;
      font-size: 120px;
      text-shadow: 0 1px 1px rgba(0,0,0,.4);
      background-color: transparent;
      border: 0;
      z-index: 10;
    }

    .carousel .item {
      height: 500px;
    }
    .carousel img {
      position: absolute;
      top: 0;
      left: 0;
      min-width: 100%;
      height: 500px;
    }

    .carousel-caption {
      background-color: transparent;
      position: static;
      max-width: 550px;
      padding: 0 20px;
      margin-top: 200px;
    }
    
    .carousel-caption .lead {
      margin: 0;
      line-height: 1.25;
      color: #fff;
      text-shadow: 0 1px 1px rgba(0,0,0,.4);
    }
    .carousel-caption .btn {
      margin-top: 10px;
    }



    </style>



<link rel="shortcut icon" href="">
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
</head>


<body id="body-index" itemscope itemtype="http://schema.org/WebPage">
<div id="wrap-index"  itemscope itemtype="http://schema.org/LocalBusiness" >
<?php $this->load->view("header"); ?>
    <div id="myCarousel" class="carousel slide">
      <div class="carousel-inner">
        <div class="item active">
          <img src="images/banner/soporte-tecnico-pymes.jpg" alt="soporte-tecnico-pymes"> 
          <div class="container">
            <div class="carousel-caption">
              <h2>Soporte T&eacute;cnico para PYMES.</h2>
              <p class="lead">El uso de las tecnolog&iacute;as es fundamental en estos tiempos para el creciemiento estructurado y organizado de las empresas. Implementamos recursos segun sea el tama&ntilde;o de la necesidad</p>
              <a class="btn btn-large btn-primary" href="#">Ver más</a>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="images/banner/ecommerce-puerto-vallarta.jpg" alt="ecommerce-puerto-vallarta">
          <div class="container">
            <div class="carousel-caption">
              <h2>Empieza tu propio negocio por internet.</h2>
              <p class="lead">Los tiempos cambian.El comercio en internet es una realidad al alcance de todos. Ponemos la tecnologia de las grandes empresas de venta online a la altura sus posibilidades.</p>
              <a class="btn btn-large btn-primary" href="#">Ver más</a>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="images/banner/venta-de hardware-puerto-vallarta.jpg" alt="venta-de hardware-puerto-vallarta">
          <div class="container">
            <div class="carousel-caption">
              <h2>Visita nuestra tienda online.</h2>
              <p class="lead">Contamos con Tarjetas Madres, Fuentes de Poder, Memorias RAM, Discos duros y más, consulta nuestro cat&aacute;logo </p>
              <a class="btn btn-large btn-primary" href="#">Visitar Tienda</a>
            </div>
          </div>
        </div>
		  <div class="item">
          <img src="images/banner/diseño-web-puerto-vallarta.jpg" alt="diseño-web-puerto-vallarta">
          <div class="container">
            <div class="carousel-caption">
              <h2>Desarrollo de Apps y Diseño Web.</h2>
              <p class="lead">Diseño de sitios web adaptado a dispositivos moviles. </p>
              <a class="btn btn-large btn-primary" href="#">Ver Más</a>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div><!-- /.carousel -->



		<div id="box-intro">
			<span id="about-title">&iquest;Necesitas de la tecnolog&iacute;a para hacer crecer tu negocio?</span>
			
			<h1 class="txt-about">En Brontobyte te  brindamos soluciones de tecnologia informatica para peque&ntilde;as y medianas empresas, Servicios de Diseño Web profesional,  ademas contamos con una amplia variedad en Hardware, visita nuestra tienda online, ofrecemos nuestros servicios en la Ciudad de Puerto Vallarta y Bahia de Banderas.</h1>
				<!--<h2 class="txt-about">configuración de redes y cámaras de seguridad en Puerto Vallarta </h2>SLIDE -->
			


		</div>
<div id="box-cover-servicios">
	<div class="box-servicios-all">

		<img src="images/ico/reparacion.png"/>
		<div class="box-txt-serv-home">
		<span class="name-service-home">&iquest;Tu computadora esta lenta o le falla algo?</span>
		<span class="txt-servicios-home">Visitanos , Revisión sin ningun costo, Tambi&eacute;n contamos con atencion al publico en general.
		</span>
		</div>
	</div>	
		
	<div class="box-servicios-all">
		<img src="images/ico/config.png"/>
		<div class="box-txt-serv-home">
		<span class="name-service-home">Polizas de Soporte y Mantenimiento</span>
		<span class="txt-servicios-home">Nos ajustamos al tama&ntilde;o de su negocio. Contactenos y diganos cuales son sus necesidades.
		</span>
			</div>
	</div>	
				
	<div class="box-servicios-all">
		<img src="images/ico/actualizar.png"/>
		<div class="box-txt-serv-home">
		<span class="name-service-home">Outsourcing TI</span>
		<span class="txt-servicios-home">Eres una empresa de TI y necesitas apoyo Outsourcing TI en el &aacute;rea de Puerto Vallarta, Bahía de Banderas y Cabo Corrientes. Podemos ayudarte;
		</span>
			</div>
	</div>	
	
	<div class="box-servicios-all">
		<img src="images/ico/dispositivo.png"/>
		<div class="box-txt-serv-home">
			<span class="name-service-home">Desarrollo de Sitios Web Profesionales</span>
			<span class="txt-servicios-home">Construimos tu sitio Web  usando los ultimos estandares  y tecnolog&iacute;as Garantizando su optimo funcionamiento en todos los nuevos dispositivos mobiles.
			</span>
		</div>
	</div>	






</div>
<div class="box-tsection">
<span class="titu-section">Novedades en Hardware</span>
<a  id="url-tienda" href="<?php echo base_url();?>index.php/catalogo">Visita nuestra tienda online.</a>
<span id="bol"></span>
</div>


<section id="cover-articulos-index" itemscope itemtype="http://schema.org/Store">
	
	
	<article itemscope itemtype="http://schema.org/Product"  class="box-index-articulos">
		
		<div class="box-img-article">
			<a itemprop="url"  href="#"><img itemprop="image"  src="images/img-promo/computadora.jpg"></a>
			
		</div>
		<span class="txt-articulos-index"  itemprop="name">HP SLATE 21-K100 TABLET </span>
		<hr class="hr-art"/>
			<a  class="btn-mas" href="detalle-item.php">
					<span class="fx-btn">
					 </span>
					 <img class="ico-btn" src="images/ico/point.png"/>
				<span class="txt-btn-mas">Más info</span>
			
			</a>

	
	</article>
	<article itemscope itemtype="http://schema.org/Product"  class="box-index-articulos">
		
		<div class="box-img-article">
			<a itemprop="url"  href="#"><img itemprop="image"  src="images/img-promo/computadora.jpg"></a>
			
		</div>
		<span class="txt-articulos-index"  itemprop="name">HP SLATE 21-K100 TABLET </span>
		<hr class="hr-art"/>
		
		<a  class="btn-mas" href="detalle-item.php">
		    <span class="fx-btn">
			 </span>
			 <img class="ico-btn" src="images/ico/point.png"/>
		<span class="txt-btn-mas">Más info</span>
	
	   </a>

	
	</article>
	<article itemscope itemtype="http://schema.org/Product"  class="box-index-articulos">
		
		<div class="box-img-article">
			<a itemprop="url"  href="#"><img itemprop="image"  src="images/img-promo/computadora.jpg"></a>
			
		</div>
		<span class="txt-articulos-index"  itemprop="name">HP SLATE 21-K100 TABLET </span>
		<hr class="hr-art"/>
		<a  class="btn-mas" href="detalle-item.php">
		    <span class="fx-btn">
			 </span>
			 <img class="ico-btn" src="images/ico/point.png"/>
		<span class="txt-btn-mas">Más info</span>
	
	  </a>
	
	</article>

<!--
	<article itemscope itemtype="http://schema.org/Product"  class="box-index-articulos">
		<h3 class="h3-articulos-index">COTIZA AHORA</h3>


		<form action="enviar.php" method="post" name="form-cotizar" target="_self" id="form-cotizar">
					<label>Nombre</label><br/>
					<input name="buscar" type="text" id="cotizar" /><br/>
					<label>Telefono o Mail</label><br/>
					<input name="buscar" type="text" id="cotizar" /><br/>
					<label>Producto</label><br/>
					<label>
					<select name="producto-cotizar"  id="producto-cotizar">
						<option>:: Selecciona una Categoria::</option>
					  <option>Tarjetas madre</option>
					  <option>Cargadores</option>
					  </select><br />
					  <label>Mensaje</label><br />
				<textarea name="solicitud" cols="25" rows="4"  id="solicitud" placeholder="Escribe las caracteristicas del producto..."></textarea><br />
					</label>
					
				
				<input type="submit" name="button" id="btn-coti" class="boton-articulos"  value="ENVIAR" />
				
				</form>
			
	</article>
-->
		
		
</section>
	<?php include('footer.php'); ?>
</div>
<!-- bootstrop-->
 <script src="http://code.jquery.com/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
     <script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>
 <!-- bootstrop-->
 
 
<!-- slider-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
		<script src="js/jquery.blueberry.js"></script>

		<script>
		$(window).load(function() {
			$('.blueberry').blueberry();
		});
		</script>
<!-- slider-->
</body>


</html>