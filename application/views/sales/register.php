<?php $this->load->view("partial/header"); ?>
<div id="page_title2" style="margin-bottom:8px;"><?php echo $this->lang->line('sales_register'); ?></div>
<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}

if (isset($warning))
{
	echo "<div class='warning_mesage'>".$warning."</div>";
}

if (isset($success))
{
	echo "<div class='success_message'>".$success."</div>";
}
?>
<div id="register_wrapper">

<?php echo form_open("sales/add",array('id'=>'add_item_form')); ?>
<label id="item_label" for="item">

<?php
if($mode=='sale')
{
	echo $this->lang->line('sales_find_or_scan_item');
}
else
{
	echo $this->lang->line('sales_find_or_scan_item_or_receipt');
}
?>
</label>
<?php echo form_input(array('name'=>'item','id'=>'item','size'=>'40'));?>

<div id="cover_btn_new_ctas_cre">

</div>

</form>
<section id="cover_register">

<div id="filas-head">
<div class="head-filas"><?php echo $this->lang->line('sales_quantity'); ?></div>
<div class="head-filas"><?php echo $this->lang->line('common_delete'); ?></div>
<div class="head-filas"  id="item-name"> <?php echo $this->lang->line('sales_item_name'); ?></div>

<div class="head-filas"><?php echo $this->lang->line('sales_edit'); ?></div>
</div>

<?php
if(count($cart)==0)
{
?>

<div class='warning_message' id='warning_message_reg'><?php echo $this->lang->line('sales_no_items_in_cart'); ?></div>

<?php
}

///////////////////isset($item_kit_id)
    
//////////////////////////////////////////////
///////////////////////////////////////////////
////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
else
    
{
	foreach(array_reverse($cart, true) as $line=>$item)
	{
		$cur_item_info = $this->Item->get_info($item['item_id']);
		echo form_open("sales/edit_item/$line");
	?>
		<div id="filas-contenido">
                    
		
		<div class="soloVisibleResponsivamente2">Cantidad</div>
           <div class="filas-items-reg">
		   
		   <?php
			echo form_input(array('name'=>'quantity','value'=>$item['quantity'],'size'=>'2'));
		  ?>
			</div>
			<div class="soloVisibleResponsivamente2">Borrar</div>
			<div class="filas-items-reg"><?php
        	
        		 echo anchor("sales/delete_item/$line",'['.$this->lang->line('common_delete').']');
        
		?></div>
		<div class="soloVisibleResponsivamente2">Nombre del Producto</div>
		<div  id="name-reg" class="filas-items-reg">	<span><?php echo $item['name']; ?><br /> [<?php echo $cur_item_info->quantity; ?>almacen]	</span></div>



		
			
		
			
			<?php echo form_input(array('name'=>'price','value'=>$item['price'],'type'=>'hidden')); ?>
		<?php echo form_input(array('name'=>'discount','value'=>$item['discount'],'type'=>'hidden'));?>
		
		<div class="soloVisibleResponsivamente2">Modificar</div>
		<div class="filas-items-reg fila-edit-item"><?php echo form_submit("edit_item", $this->lang->line('sales_edit_item'));?></div>
		</div>
		
		
		
		<div class="cover_descrip_item">
	
		<p class="description_item_txt">

		<?php
        	if($item['allow_alt_description']==1)
        	{
        		echo form_input(array('name'=>'description','value'=>$item['description'],'size'=>'20'));
        	}
        	else
        	{
				if ($item['description']!='')
				{
					echo $item['description'];
        			echo form_hidden('description',$item['description']);
        		}
        		else
        		{
        			//echo 'None';
        			echo form_hidden('description','');
        		}
        	}
		?>
		</p>
		<td style="color:#FFF";>
		<?php
        	if($item['is_serialized']==1)
        	{
				echo $this->lang->line('sales_serial').':';
			}
		?>
		</td>
		<td colspan=3 style="text-align:left;">
		<?php
        	if($item['is_serialized']==1)
        	{
        		echo form_input(array('name'=>'serialnumber','value'=>$item['serialnumber'],'size'=>'20'));
			}
			else
			{
				echo form_hidden('serialnumber', '');
			}
		?>
		</td>


		</div>
		<tr style="height:3px">
		<td colspan=8 style="background-color:white"> </td>
		</tr>		</form>
	<?php
	}
}
?>

</section>

</div>




<div id="overall_sale">
		<div class="cover_overall_process">
			<div id="box-delete-proverdor">
			   <?php
			if(isset($customer))
			{
				echo $this->lang->line("sales_customer").': <span><b>'.$customer. '</b></span>';
				echo anchor("sales/remove_customer",'['.$this->lang->line('common_remove').' '.$this->lang->line('customers_customer').']'); ?>
			</div>
			
			<div id="finish_sale">

	<div   id="Payment_Types" >

			<div class="cover_label_input_ptypes">
			
			<?php echo form_open("sales/complete",array('id'=>'finish_sale_form')); ?>
				<label id="comment_label" for="comment"><?php echo $this->lang->line('common_comments'); ?>:</label>
				<?php echo form_textarea(array('name'=>'comment', 'id' => 'comment', 'value'=>$comment,'rows'=>'4','cols'=>'23'));?>
				</div>
				<div id="cover_btns_venta">
					<div class='btn_ok_venta' id='btn_comple_sale_group'><span><?php echo $this->lang->line('sales_complete_sale')?></span></div>
				</form>	
				<?php echo form_open("sales/cancel_sale",array('id'=>'cancel_sale_form')); ?>
				<div class="btn_ok_venta" id="btn_cancel_sale_group">
					<span><?php echo $this->lang->line('sales_cancel_sale'); ?></span>
				</div>
				</form>
				
				
				</div>
				
				
			
			</div>
				
		</div>

			
			</div>	
		<?php 
		}
		else
		{
			echo form_open("sales/select_customer",array('id'=>'select_customer_form')); ?>
			<label class="overall_label"   for="customer"><?php echo $this->lang->line('sales_select_customer'); ?></label>
			<?php echo form_input(array('name'=>'customer','id'=>'input_empieza','size'=>'30','value'=>$this->lang->line('sales_start_typing_customer_name')));?>
			</form>
		
		</div>
		<div class="cover_overall_process" id="box-or-btn">
			<h3 id="or"><?php echo $this->lang->line('common_or'); ?></h3>
			<?php echo anchor("customers/view/-1/width:350",
			"<div class='btn_ok'  id='btn_new_cliente'><span>".$this->lang->line('sales_new_customer')."</span></div>",
			array('class'=>'thickbox none','title'=>$this->lang->line('sales_new_customer')));
			?>
		</div>
		<?php
	}
	?>
	
</div>             

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $("#item").autocomplete('<?php echo site_url("sales/item_search"); ?>',
    {
    	minChars:0,
    	max:50,
    	selectFirst: false,
       	delay:1,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#item").result(function(event, data, formatted)
    {	
		$("#add_item_form").submit();
		$('#item').attr('value','');
    });

	$('#item').focus();

	

	$('#item,#input_empieza').click(function()
    {
    	$(this).attr('value','');
    });

    $("#input_empieza").autocomplete('<?php echo site_url("sales/customer_search"); ?>',
    {
    	minChars:0,
    	delay:0,
    	max:50,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#input_empieza").result(function(event, data, formatted)
    {
		$("#select_customer_form").submit();
    });

    
	
	$('#comment').change(function() 
	{
		$.post('<?php echo site_url("sales/set_comment");?>', {comment: $('#comment').val()});
	});
	
	$('#email_receipt').change(function() 
	{
		$.post('<?php echo site_url("sales/set_email_receipt");?>', {email_receipt: $('#email_receipt').is(':checked') ? '1' : '0'});
	});
	
	
    $("#btn_comple_sale_group").click(function()
    {
    	if (confirm('<?php echo $this->lang->line("sales_confirm_finish_sale"); ?>'))
    	{
    		$('#finish_sale_form').submit();
    	}
    });

	$("#suspend_sale_button").click(function()
	{
		if (confirm('<?php echo $this->lang->line("sales_confirm_suspend_sale"); ?>'))
    	{
			$('#finish_sale_form').attr('action', '<?php echo site_url("sales/suspend"); ?>');
    		$('#finish_sale_form').submit();
    	}
	});

    $("#btn_cancel_sale_group").click(function()
    {
    	if (confirm('<?php echo $this->lang->line("sales_confirm_cancel_sale"); ?>'))
    	{
    		$('#cancel_sale_form').submit();
    	}
    });

	$("#add_payment_button").click(function()
	{
	   $('#add_payment_form').submit();
    });

	$("#payment_types").change(checkPaymentTypeGiftcard).ready(checkPaymentTypeGiftcard)
});

function post_item_form_submit(response)
{
	if(response.success)
	{
		
		$("#item").attr("value",response.item_id);
		
		$("#add_item_form").submit();
		
	}
}

function post_person_form_submit(response)
{
	if(response.success)
	{
		$("#input_empieza").attr("value",response.person_id);
		$("#select_customer_form").submit();
	}
}

function checkPaymentTypeGiftcard()
{
	if ($("#payment_types").val() == "<?php echo $this->lang->line('sales_giftcard'); ?>")
	{
		$("#amount_tendered_label").html("<?php echo $this->lang->line('sales_giftcard_number'); ?>");
		$("#amount_tendered").val('');
		$("#amount_tendered").focus();
	}
	else
	{
		$("#amount_tendered_label").html("<?php echo $this->lang->line('sales_amount_tendered'); ?>");		
	}
}

</script>

</div>

</div>


<div id="box-footer">
<?php $this->load->view("partial/footer"); ?>

</div>
