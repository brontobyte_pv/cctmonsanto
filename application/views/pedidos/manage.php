<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
var $j =jQuery.noConflict();
$j(document).ready(function() 
{ 
    init_table_sorting();
    enable_select_all_citas();
	enable_checkboxes_citas();
    enable_row_selection_citas();
    enable_search_citas('<?php echo site_url("$controller_name/")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    
    enable_delete_citas('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');
}); 

function init_table_sorting()
{
	//Only init if there is more than one row
	if($j('.tablesorter tbody tr').length >1)
	{
		$j("#sortable_table").tablesorter(
		{ 
			sortList: [[1,0]], 
			headers: 
			{ 
				0: { sorter: false}, 
				6: { sorter: false} 
			} 

		}); 
	}
}

function post_person_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);	
	}
	else
	{
		//This is an update, just update one row
		if($j.inArray(response.appointment_id,get_visible_checkbox_ids_citas()) != -1)
		{
			update_row_citas(response.appointment_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);	
			
		}
		else //refresh entire table
		{
			do_search_citas(true,function()
			{
				//highlight new row
				hightlight_row(response.appointment_id);
				set_feedback(response.message,'success_message',false);		
			});
		}
	}
}
</script>

<div id="title_bar">
	<div id="title" ><?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?></div>
	<div id="cover_buttons_top_manage">
		<?php echo anchor("$controller_name/view/-1/width:$form_width",
		"<div class='buttons_top_manage'><img src='".base_url()."images/ico/ico-pedidos.png'/><span>".$this->lang->line($controller_name.'_new')."</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line($controller_name.'_new')));
		?>
	</div>
</div>

<div id="table_action_header">
	<ul>
		
		<li  id="box-buscar">
		
		<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
		<div id="datetimepicker2" class="input-append date">
	<?php echo form_input(array(
		'name'=>'search',
		'id'=>'search',
		'data-format'=>'yyyy/MM/dd',
		'value'=>''));?>
	<span class="add-on" id="icono-calendar-pedidos">
      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	  <img src="images/ico/ico-appo2.png"/>
      </i>
	   
    </span>
	</div>
		</form>
		</li>
		
		<li id="btn-borrar-buscar" ><span><?php echo anchor("$controller_name/cancel",$this->lang->line("common_delete"),array('id'=>'delete')); ?></span></li>
		<li  id="btn-mail-buscar" ><span><a href="#" id="email"><?php echo $this->lang->line("common_email");?></a></span></li>
		
	</ul>
</div>
<div class="box-pagination">
<?php echo $this->pagination->create_links();?>
</div>
<div id="table_holder">
<?php echo $manage_table; ?>
</div>
<div id="feedback_bar"></div>
</div>
</div>

<script type="text/javascript">

  $(function() {
    $('#datetimepicker2').datetimepicker({
      language: 'pt-BR',
	  pick12HourFormat: true,
	  pickSeconds: false,
    });
  });
  
</script>
<div id="box-footer">
<?php $this->load->view("partial/footer"); ?>

</div>

