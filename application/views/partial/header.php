<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<base href="<?php echo base_url();?>" />
    <link rel="shortcut icon" href="<?php echo base_url();?>images/menubar/cash_control.ico" />
	<title><?php echo $this->lang->line('module_'.$controller_name); ?></title>
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/ospos.css" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/style.css" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/ospos_print.css"  media="print"/>
	
    <script src="<?php echo base_url();?>js/jquery-1.3.1.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo base_url();?>js/slider.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo base_url();?>js/jquery-1.2.6.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.color.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.metadata.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.form.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.tablesorter.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.ajax_queue.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.bgiframe.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.autocomplete.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.validate.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.jkey-1.1.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/thickbox.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/common.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/manage_tables.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/swfobject.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/date.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/datepicker.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap.css">
	<script src="http://code.jquery.com/jquery.js"></script>
	
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,300italic,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Cabin:400,600,500' rel='stylesheet' type='text/css'>
	
<style type="text/css">
html {
    overflow: auto;
}
</style>
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/dispositivos_cct.css" />
</head>
<body>
<div id="header-home">
<div id="menubar">

	
			<div class="accordion" id="accordion">
					  <div class="accordion-group">
					
						<div class="accordion-heading">
								
						  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#ans2">
						  	 
							<div id="box-ico-menu">
							 <span class="name-menu">MENU</span>
							 <img class="flecha-leer" src="<?php echo base_url();?>images/ico/menu-desp.png">
						
							</div>
							 
							

						  </a>
						</div>
						  <div id="menubar_footer">
									<?php echo $this->lang->line('common_welcome')." $user_info->first_name $user_info->last_name! | "; ?>
									<?php echo anchor("home/backup",'Respaldar'); ?><br>
									<?php echo anchor("home/logout",$this->lang->line("common_logout")); ?>
									
									
							</div>
							
						    <div id="menubar_date">
								<?php echo date('F d, Y h:i a') ?>
							</div>
							
							
							
							
							
						<div id="ans2" class="accordion-body collapse">
						  <div class="accordion-inner">
			
			<div id="menubar_navigation">
			<?php
			foreach($allowed_modules->result() as $module)
			{
			?>
			<div class="menu_item">
				<a  href="<?php echo site_url("$module->module_id");?>"  >
				<img src="<?php echo base_url().'images/menubar/'.$module->module_id.'.png';?>" border="0" alt="Menubar Image" /></a><br />
				<a id="text-menu-a"  href="<?php echo site_url("$module->module_id");?>"><?php echo $this->lang->line("module_".$module->module_id) ?></a>
                                 
                        
             </div>
			<?php
			}
			?>
		</div>
		
						</div>
					  </div>
					</div>
		
	

 
	</div>
	</div>
	</div>
  <!-- bootstrop  accordion-->
 <script src="<?php echo base_url();?>js/bootstrap.js"></script>
 <!-- bootstrop-->
<div id="content_area_wrapper">
<div id="content_area">
