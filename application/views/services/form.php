<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('services/save/'.$info->service_id,array('id'=>'form_services'));
?>
<fieldset id="new_service_info">
<legend class="name-forms-popup"><?php echo $this->lang->line("services_nuevo_servicio"); ?></legend>

<div class="field_row_name_client">
<?php echo form_label($this->lang->line('cobros_cliente').':', 'nom_cliente',array('class'=>'name-client-field')); ?>
	
	<?php echo form_label($customer, 'nom_cliente',array('class'=>'name-client-field')); ?>
	<?php echo form_input(array(
		'name'=>'nom_cliente',
		'id'=>'nom_cliente',
		'type'=>'hidden',
		'value'=>$customer)
	);?>
	
	
</div>


<div class="field_row clearfix">
<?php echo form_label($this->lang->line('services_motivo'), 'motivo', array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'motivo',
		'id'=>'motivo')
	);?>
	</div>
</div>
<div id="productos_prestamo">

	<div class="prestamo-box">
		<div class="head-prestamo"><?php echo $this->lang->line('common_delete');?></div>
		<div class="head-prestamo" id="name-pro-pres"><?php echo $this->lang->line('item_kits_item');?></div>
		<div class="head-prestamo"><?php echo $this->lang->line('item_kits_quantity');?></div>
	</div>
	
	<?php 

	foreach ($this->Item_kit_items->get_info($items_info->service_id) as $productos_prestamo) { ?>
		<div class="prestamo-box">
		
			<?php
			
			$item_info = $this->Item->get_info($productos_prestamo['item_id']);
			?>
			<div class="head-res-prestamo"><a href="#" onclick='return deleteItemKitRow(this);'>X</a>
			</div>
			<div class="head-res-prestamo" id="name-pro-pres-head"><?php echo $item_info->name; ?></div>
			<div class="head-res-prestamo"> <input class="quantity" id="quantity" type='text' size='3' name=productos_prestamo[<?php echo $productos_prestamo['item_id'] ?>] value='<?php echo $productos_prestamo['quantity'] ?>'/>
			</div>
		</div>
		
	<?php } ?>
	
</div>











<div class="field_row clearfix">
<?php echo form_label($this->lang->line('services_estado').':', 'estado', array('class'=>'required')); ?>
    <div class='form_field'>
	<?php 
		echo form_dropdown('estado',$estado,'');?>
	</div>
</div>	
<div class="field_row_name_client">
  <?php echo form_label($this->lang->line('services_asignado').':', 'nom_asignado', array('class'=>'required')); ?>
   
	<?php echo form_label($nom_asignado, 'nom_asignado',array('class'=>'name-client-field')); ?>
	<?php echo form_input(array(
		'name'=>'nom_asignado',
		'id'=>'nom_asignado',
		'type'=>'hidden',
		'value'=>$nom_asignado)
	);?>
	
	
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button')
);

?>
        
	


</fieldset>
<?php
echo form_close();
?>

<script type='text/javascript'>
var i=0;
$("#motivo").autocomplete('<?php echo site_url("items/item_search"); ?>',
{
	minChars:0,
	max:100,
	selectFirst: false,
   	delay:10,
	formatItem: function(row) {
		return row[1];
	}
});

$("#motivo").result(function(event, data, formatted)
{
	
	$("#motivo").val("");
	i++;
	if ($("#productos_prestamo_"+data[0]).length ==1)
	{
		$("#productos_prestamo_"+data[0]).val(parseFloat($("#productos_prestamo_"+data[0]).val()) + 1);
	}
	else
	{
		
		
		$("#productos_prestamo").append("<div class='prestamo-box'><div class='head-res-prestamo'><a href='#' onclick='return deleteItemKitRow(this);'>X</a></div><div id='name-pro-pres' class='head-res-prestamo'>"+data[1]+"</div><div class='head-res-prestamo'><input class='quantity' id='productos_prestamo_"+data[0]+"' type='text' size='3' name=productos_prestamo["+data[0]+"] value='1'/></div></div>");
		
		
	}
});

//validation and submit handling
$(document).ready(function()
{
	$('#form_services').validate({
		submitHandler:function(form)
		{
			tb_remove();
			post_form_submit();
			$(form).ajaxSubmit({
			success:function(response)
			{
				
			}
			
			,
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			
    		
			
   		},
		messages: 
		{
     		
     		
		}
	});
});
function deleteItemKitRow(link)
{
	$(link).parent().parent().remove();
	return false;
}
</script>
