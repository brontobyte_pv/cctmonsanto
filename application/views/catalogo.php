<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="UTF-8">
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex, follow" />
<meta name="description" content="Venta de hardware y Reparación de equipo computacional, Configuraci&oacute;n de Redes, C&aacute;maras de Seguridas... Reparaci&oacute;n de Computadoras en Puerto Vallarta">
<meta name="keywords" content="Venta de hardware y Reparaci&oacute;n Computadoras en Puerto Vallarta, Configuración de Redes y C&aacute;maras de Seguridas, Refacciones de Computadoras, cargadores para laptop accesorios para celulares , venta de computadoras, Instalaci&oacute;n de programas, Intalaci&oacute;n de Aplicaciones">
<meta name="author" content="Brontobyte Computaci&oacute;n">

<title>Cat&aacute;logo de Productos</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-contenido.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/ responsive.css" />   
<link rel="shortcut icon" href="<?php echo base_url();?>images/ico/ico-logo.ico">
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
</head>

<body>
<div id="wrap-tienda" class="wrapper" itemscope itemtype="http://schema.org/LocalBusiness" >
<?php $this->load->view("header"); ?>
		<div id="box-subtitulo-secciones">
				<h3 class="h3-subtitulo-secciones"><a  class="head-a-ubi" href="<?php echo base_url();?>">Pagina de inicio / </a><span   class="head-ubica2" itemprop="keywords" >Tienda online</span></h3>
		</div>
	<section itemscope itemtype="http://schema.org/Store" class="contenido-secciones" id="seccion-catal" >
	   
				<div class="box-nombre-seccion">
					<span class="subtitulos-seccion">TIENDA ONLINE</span> 
					<p class="p-intro">
					Brontobyte tienda Software & Hardware , en esta seccion podras encontrar nuestra variedad de productos desde accesorios de computadora hasta tarjetas madre, memorias RAM y más.
					</p>
				</div>
			<div id="box-catego-tienda">		
			<?php echo $manage_table; ?>
			</div>	
	</section>
<?php include('footer.php'); ?>
</div>

</body>