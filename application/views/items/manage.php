<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
$(document).ready(function()
{
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');
    enable_bulk_edit('<?php echo $this->lang->line($controller_name."_none_selected")?>');

    $('#generate_barcodes').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo $this->lang->line('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','index.php/items/generate_barcodes/'+selected.join(','));
    });

    $("#low_inventory").click(function()
    {
    	$('#items_filter_form').submit();
    });

    $("#is_serialized").click(function()
    {
    	$('#items_filter_form').submit();
    });

    $("#no_description").click(function()
    {
    	$('#items_filter_form').submit();
    });

});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				0: { sorter: false},
				8: { sorter: false},
				9: { sorter: false}
			}

		});
	}
}

function post_item_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.item_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.item_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				hightlight_row(response.item_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}

function post_bulk_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		var selected_item_ids=get_selected_values();
		for(k=0;k<selected_item_ids.length;k++)
		{
			update_row(selected_item_ids[k],'<?php echo site_url("$controller_name/get_row")?>');
		}
		set_feedback(response.message,'success_message',false);
	}
}

function show_hide_search_filter(search_filter_section, switchImgTag) {
        var ele = document.getElementById(search_filter_section);
        var imageEle = document.getElementById(switchImgTag);
        var elesearchstate = document.getElementById('search_section_state');
        if(ele.style.display == "block")
        {
                ele.style.display = "none";
				imageEle.innerHTML = '<img src=" <?php echo base_url()?>images/menubar/plus.png" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-5px;" >';
                elesearchstate.value="none";
        }
        else
        {
                ele.style.display = "block";
                imageEle.innerHTML = '<img src=" <?php echo base_url()?>images/menubar/minus.png" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-5px;" >';
                elesearchstate.value="block";
        }
}

</script>





<div id="title_bar">
	<div id="title" > <?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?></div>
	<div id="cover_buttons_top_manage">
		<?php echo anchor("$controller_name/view/-1/width:$form_width",
		"<div class='buttons_top_manage' > <img src='".base_url()."images/ico/items-ico.png'/><span>".$this->lang->line($controller_name.'_new')."</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line($controller_name.'_new')));
		?>
		
	</div>
</div>


<div id="table_action_header">
	<ul>
	<li  id="box-buscar" >
		<img src="images/ico/icon-lupa.png"/>
		<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
		<input type="text" name ='search' id='search'/>
		</form>
	</li>
	
		<li id="btn-borrar-buscar" ><span><?php echo anchor("$controller_name/delete",$this->lang->line("common_delete"),array('id'=>'delete')); ?></span>
		</li>
		<li  id="btn-editar-buscar" ><span><?php echo anchor("$controller_name/bulk_edit/width:$form_width",$this->lang->line("items_bulk_edit"),array('id'=>'bulk_edit','title'=>$this->lang->line('items_edit_multiple_items'))); ?></span></li>
	
		
	</ul>
</div>

<div id="opc_buscar">
		<div id="titleTextImg" ">
			<div style="float:left;vertical-align:text-top;">Más opciones de Busqueda:</div>
			<a id="imageDivLink" href="javascript:show_hide_search_filter('search_filter_section', 'imageDivLink');">
			<img src="
			<?php echo isset($search_section_state)?  ( ($search_section_state)? base_url().'images/menubar/minus.png' : base_url().'images/menubar/plus.png') : base_url().'images/menubar/plus.png';?>"></a>
		</div>

		<div id="search_filter_section" style="display: <?php echo isset($search_section_state)?  ( ($search_section_state)? 'block' : 'none') : 'none';?>;">
			<?php echo form_open("$controller_name/refresh",array('id'=>'items_filter_form')); ?>
			<?php echo form_label($this->lang->line('items_low_inventory_items').' '.':', 'low_inventory');?>
			<?php echo form_checkbox(array('name'=>'low_inventory','id'=>'low_inventory','value'=>1,'checked'=> isset($low_inventory)?  ( ($low_inventory)? 1 : 0) : 0)).' | ';?>
			<?php echo form_label($this->lang->line('items_serialized_items').' '.':', 'is_serialized');?>
			<?php echo form_checkbox(array('name'=>'is_serialized','id'=>'is_serialized','value'=>1,'checked'=> isset($is_serialized)?  ( ($is_serialized)? 1 : 0) : 0)).' | ';?>
			
			<input type="hidden" name="search_section_state" id="search_section_state" value="<?php echo isset($search_section_state)?  ( ($search_section_state)? 'block' : 'none') : 'none';?>" />
			</form>
			
		</div>
</div>

<div id="table_holder"> 
<?php echo $manage_table; ?>
</div>
<div class="box-pagination">
<?php echo $this->pagination->create_links();?>
</div>
<div id="feedback_bar"></div>
</div>
</div>
<div id="box-footer">
<?php $this->load->view("partial/footer"); ?>

</div>


