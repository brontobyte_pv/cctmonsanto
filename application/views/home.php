<?php $this->load->view("partial/header"); ?>
<div id="home_module_list">
<div id="manual-ayuda">
<img id="ico-ayuda" src="<?php echo base_url();?>images/menubar/ayuda.png"></img>
<a href="manual_cash_control.pdf"> Ver Manual</a>
</div>

<h3 id="h3bienvenido">Bienvenido a Sistema de Almacén MSP</h3>
	<?php
	foreach($allowed_modules->result() as $module)
	{
	?>
	<div class="module_item">
		<a id="url-home-img-module" href="<?php echo site_url("$module->module_id");?>">
		<img src="<?php echo base_url().'images/menubar/gray-icon/'.$module->module_id.'.png';?>" border="0" alt="Menubar Image" /></a><br />
		<a id="a-name-catego"  href="<?php echo site_url("$module->module_id");?>"><?php echo $this->lang->line("module_".$module->module_id) ?></a>
		 - <?php echo $this->lang->line('module_'.$module->module_id.'_desc');?>
	</div>
	<?php
	}
	?>
</div>
</div>
</div>
<div id="box-footer">

<?php $this->load->view("partial/footer"); ?>

</div>

