<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="UTF-8">
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex, follow" />
<meta name="description" content="Venta de hardware y Reparación de equipo computacional, Configuraci&oacute;n de Redes, C&aacute;maras de Seguridas... Reparaci&oacute;n de Computadoras en Puerto Vallarta">
<meta name="keywords" content="Venta de hardware y Reparaci&oacute;n Computadoras en Puerto Vallarta, Configuración de Redes y C&aacute;maras de Seguridas, Refacciones de Computadoras, cargadores para laptop accesorios para celulares , venta de computadoras, Instalaci&oacute;n de programas, Intalaci&oacute;n de Aplicaciones">
<meta name="author" content="Brontobyte Computaci&oacute;n">

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>Brontobyte Computación, Reparacion de computadoras en Puerto vallarta</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-contenido.css">

<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,200,500,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>

<script src="http://code.jquery.com/jquery.js"></script>

<link rel="shortcut icon" href="<?php echo base_url();?>images/ico/ico-logo.ico">

</head>


<body>
	<div class="wrapper"  id="wrap-contac" itemscope itemtype="http://schema.org/LocalBusiness" >
		<?php $this->load->view("header"); ?>
		<section class="contenido-secciones" id="seccion-contac" itemprop="location" itemscope itemtype="http://schema.org/Place">
		<div id="box-subtitulo-secciones">
		
		       <h3 class="h3-subtitulo-secciones">
				
				<a class="head-a-ubi" href="<?php echo base_url();?>">Pagina de inicio / </a>
				
				<span class="head-ubica2"  itemprop="keywords" >
				Contacto 
				</span>
				
				
				
				</h3>
				
				
			</div>
		<div class="box-nombre-seccion">
					<span class="subtitulos-seccion">Contactanos</span> 
					
				</div>
			<article id="box-mapa" itemprop="hasMap">
			<iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2309.8401896503183!2d-105.23348262698363!3d20.602473651477847!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xabc46ef78a0bec4b!2sBrontobyte+Computaci%C3%B3n!5e1!3m2!1ses!2smx!4v1409853195728" width="1010" height="250" frameborder="0" style="border:0"></iframe>
			</article>
			
		 <article class="cover-info-con">
				<div class="box-infor">
					<img class="img-inf" src="<?php echo base_url();?>images/ico/ico-correo.png"/>
					<span>info@brontobytemx.com</span>
				</div>	
				
				<div class="box-infor">
					<img class="img-inf" src="<?php echo base_url();?>images/ico/ico-phone.png"/>
					<span>322 158 46 00</span>
				</div>
				
				<div class="box-infor">
					<img class="img-inf" src="<?php echo base_url();?>images/ico/ico-ubicacion.png"/>
					<span>Venustiano Carranza #178
					Col.Emiliano Zapata.</span>
				</div>
			</article>
			
			
			<article id="box-form-contac">
				
				<span id="titulo-form">Contacto Form</span>
				
		<!      <form  action="formulario.php" method="post" name="form-contacto" target="_self" id="formulario-contacto">
						<?php $attributes = array('id' => 'form-contacto'); echo form_open('front/contacto',$attributes); ?>
						<div class="cover-inputs-contacto">
						    <?php echo form_error('nombre'); ?>
							<input class="caja-form" name="nombre" type="text" id="name"  placeholder="NOMBRE *: " value="<?php echo set_value('nombre'); ?>"/>
							<?php echo form_error('correo'); ?>
							<input class="caja-form" name="correo" type="text" id="correo"  placeholder="CORREO *:" value="<?php echo set_value('correo'); ?>"/>
							<?php echo form_error('asunto'); ?>
							<input  class="caja-form" name="asunto" type="text" id="asunto" placeholder="ASUNTO *:" value="<?php echo set_value('asunto'); ?>"/>
						</div>
						<?php echo form_error('mensaje'); ?>
						<textarea name="mensaje" cols="30" rows="7"  id="mensaje-contacto" placeholder="ESCRIBE AQUÍ TU MENSAJE..." ><?php echo set_value('mensaje'); ?></textarea>
						<div id="img-captcha"><?php echo $img; ?></div>
						<?php echo $error_msj; ?>
						<input type="text" placeholder="Introduce el codigo"name='captcha'>
						<input type="submit" name="button"   id="btn-contact" value="ENVIAR MENSAJE" />
						
						 
					
					</form>
			
			</article>

			
		
		</section>
		<?php include('footer.php'); ?>
	</div>


</body>