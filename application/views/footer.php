
<span class="ir-arriba icon-arrow-up2">^</span>
<footer>

	<article  itemscope itemtype="http://schema.org/LocalBusiness" class="box-pie"  >
			<img  itemprop="image"class="img-pie" src="<?php echo base_url();?>images/img-footer/bronto-gris.png"/>
			<p class="txt-somos"> <span itemprop="name">Brontobyte</span> es 
			una empresa dedicada  a la  <span itemprop="keyword">venta de hardware</span> y <span itemprop="keyword"> soluciones TI para toda necesidad</span> , con presencia en la ciudad de Puerto vallarta, Jalisco, M&eacute;xico
			<span class="span-pie">...</span></p>
	</article>
	<div itemprop = "ruta de navegación" class="box-pie" >
		<h5 class="titulos-pie">Tienda Online</h5>
		<ul>
		<li><a itemprop = "url" href="<?php echo base_url();?>index.php/catalogo">C&aacute;talogo de productos</a></li>

		<li><a itemprop = "url" href="catalago.php">&iquest;Como comprar?</a></li>

		<li><a  itemprop = "url" href="catalago.php">Envios de Mercanc&iacute;a</a></li>

		<li><a itemprop = "url" href="catalago.php">Registro</a></li>
		</ul>
	</div>
	<div  itemprop = "ruta de navegación" class="box-pie" >
		<h5 class="titulos-pie">Servicios</h5>
			<ul>
			<li><a  itemprop = "url" href="servicios.php">Soporte TI para PYMES </a></li>
			
			<li><a itemprop = "url" href="servicios.php">Tu propia tienda Online. </a></li>

			<li><a itemprop = "url" href="servicios.php">Desarrollo WEB.</a></li>

			<li><a  itemprop = "url"href="servicios.php">Servicio al p&uacute;blico General</a></li>
			</ul>
	</div>

	<article class="box-pie"  id="pie-contacto" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
		<h5 class="titulos-pie">Contacto</h5>
		<p id="txt-contac-pie" >
		<span  itemprop="telephone">
		TELS: 
			<br/> 
			<span class="color-p">::</span>
			(322) 158 46 00 
			<span class="color-p">::</span>
			<br/>
			<span class="color-p">::</span>
			Tienda Local (322)184 19 65
		
		</span>
		<span class="color-p">::</span> 
			<br/>
			<span itemprop="streetAddress">Venustiano Carranza #381</span>
			Col. Emiliano Zapata.
		
		</p>
	</article>

	<div class="box-copy-redes">
	
	 <h4 class="pie-copy">© Copyright Brontobyte Dise&ntilde;o Web 2016</h4>
		<a href="#"><img class="img-redes-pie" src="<?php echo base_url();?>images/ico/red-facebook.png"/></a>
			
			<a href="#"><img class="img-redes-pie" id="c-pin" src="<?php echo base_url();?>images/ico/red-pin.png"/></a>
			<a href="#"><img class="img-redes-pie" id="c-goo" src="<?php echo base_url();?>images/ico/red-google.png"/></a>
		
	</div>
</footer>

<script>
$(document).ready(function(){
 
	$('.ir-arriba').click(function(){
		$('body, html').animate({
			scrollTop: '0px'
		}, 300);
	});
 
	$(window).scroll(function(){
		if( $(this).scrollTop() > 0 ){
			$('.ir-arriba').slideDown(300);
		} else {
			$('.ir-arriba').slideUp(300);
		}
	});
 
});
</script>