<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open_multipart('item_kits/save/'.$item_kit_info->item_kit_id,array('id'=>'item_kit_form'));
?>
<fieldset id="item_kit_info">
<legend class="name-forms-popup"><?php echo $this->lang->line("item_kits_info"); ?></legend>



<div class="field_row clearfix">
  <?php  echo form_label($this->lang->line('items_item_number').':', 'name',array('class'=>'wide'));?>  
    <div class='form_field'>
	<?php echo form_input(array(
		'name'=>'kit_number',
		'id'=>'kit_number',
		'value'=>$item_kit_info->kit_number)
        );?>
	</div>
</div>


<div class="field_row clearfix">
<?php echo form_label($this->lang->line('item_kits_name').':', 'name',array('class'=>'wide required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'kit_name',
		'id'=>'kit_name',
		'value'=>$item_kit_info->name)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('item_kits_category').':', 'name',array('class'=>'wide required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'kit_category',
		'id'=>'kit_category',
		'value'=>$item_kit_info->category)
	);?>
	</div>
</div>

<div class="field_row clearfix">
  <?php  echo form_label($this->lang->line('item_kits_unit_price').':', 'kit_price',array('class'=>'wide'));?>  
    <div class='form_field'>
	<?php echo form_input(array(
		'width'=>'30px',
        'name'=>'kit_price',
		'id'=>'kit_price',
		'value'=>$item_kit_info->kit_price)
                
        );?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('item_kits_description').':', 'description',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$item_kit_info->description,
		'rows'=>'5',
		'cols'=>'17')
	);?>
	</div>
</div>

<div class="field_row_checkbox clearfix">
<?php echo form_label($this->lang->line('items_publish').':', 'is_serialized',array('class'=>'wide')); ?>

	<?php echo form_checkbox(array(
		'name'=>'publish',
		'id'=>'publish',
		 'class' => 'checkbox_form',
		'value'=>1,
		'checked'=>($item_kit_info->publicar)? 1 : 0)
	);?>
	
</div>



<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>


//validation and submit handling
$(document).ready(function()
{
	$('#item_kit_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_item_kit_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			kit_name:"required",
			kit_category:"required"
		},
		messages:
		{
			kit_name:"<?php echo $this->lang->line('items_name_required'); ?>",
			kit_category:"<?php echo $this->lang->line('items_category_required'); ?>"
		}
	});
});


</script>