<?php 
//OJB: Check if for excel export process
if($export_excel == 1){
	ob_start();
	$this->load->view("partial/header_excel");
}else{
	$this->load->view("partial/header");
} 
?>
<div id="page_title" style="margin-bottom:8px;"><?php echo $title ?></div>
<div id="page_subtitle"><?php echo $subtitle ?></div>
<div id="table_holder_report">
	<div  id="sortable_table_report">
		<div class="box_head_reports_table">
			<div class="file_report_head">
				<?php foreach ($headers as $header) { ?>
				<div class="casilla_head_report_b"><?php echo $header; ?></div>
				<?php } ?>
			</div>
		</div>
		<div class="box_result_table_reports">
			<?php foreach ($data as $row) { ?>
			<div class="file_report_result">
				<?php foreach ($row as $cell) { ?>
				<div class="casilla_result_report_b"><?php echo $cell; ?></div>
				<?php } ?>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<div id="report_summary">
<?php if($report_summary_id!='services'){?>
<?php foreach($summary_data as $name=>$value) { ?>
	<div class="summary_row"><?php echo $this->lang->line('reports_'.$name). ' Total: '.to_currency($value); ?></div>
<?php }
}?>
</div>
<?php 
if($export_excel == 1){
	$this->load->view("partial/footer_excel");
	
	
	$content = ob_end_flush();
	
	$filename = trim($filename);
	$filename = str_replace(array(' ', '/', '\\'), '', $title);
	$filename .= "_Export.xls";
	header('Content-type: application/ms-excel');
	header('Content-Disposition: attachment; filename='.$filename);
	echo $content;
	
	die();
}else{
	
?>

<script type="text/javascript" language="javascript">
function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(); 
	}
}
$(document).ready(function()
{
	init_table_sorting();
});
</script>
<?php 
} // end if not is excel export 
?>