<?php 
//OJB: Check if for excel export process
if($export_excel == 1){
	ob_start();
	$this->load->view("partial/header_excel");
}else{
	$this->load->view("partial/header");
} 
?>
<div id="page_title" style="margin-bottom:8px;"><?php echo $title ?></div>
<div id="page_subtitle"><?php echo $subtitle ?></div>

<div id="table_holder_report">
	<div  id="sortable_table_report">
		
		<div class="box_head_reports_table">
			<div class="file_report_head">
		
				<div class="casilla_head_report">+</div>
				<?php foreach ($headers['summary'] as $header) { ?>
				<div class="casilla_head_report"><?php echo $header; ?></div>
				<?php } ?>
			</div>
		</div>
		
		
		
			<?php  $count=0; foreach ($summary_data as $key=>$row) { $count++;?>
			<div class="box_result_table_reports">
			
			<div class="file_report_result">
				
				<div class="casilla_result_report"><a id="boton<?php echo $count; ?>"><?php echo $this->lang->line('reports_det');?></a></div>
				<script>
				$( document ).ready(function() {
				//Genera cuantas clases sean necesarias cada que se cargue el DOM de reportes

				$(".box_head_reports_table_detail<?php echo $count; ?>").css("display","none");
				$(".box_result_table_reports_detail<?php echo $count; ?>").css("display","none");
				});
//Este script se ejecuta cuando se le da click al elemento html que tenga el id='boton'
$("#boton<?php echo $count; ?>").click(function(){
	//Primeramente verificamos el estado de la propiedad 'display' del elemento del html con el id='box-oculto-report' y se lo asignamos a la variable 'disp'
	var disp =$(".box_head_reports_table_detail<?php echo $count; ?>").css("display");
	//Comparamos si disp esta oculto lo hacemos visible
	if (disp == 'none')
	{
		//Cambia la propiedad css del elemento html con el id encerrado en los primeros parentesis
		$(".box_head_reports_table_detail<?php echo $count; ?>").css("display","block");
		$(".box_result_table_reports_detail<?php echo $count; ?>").css("display","block");
		
	}//De lo contrario lo ocultamos
	else
	{
		//Cambia la propiedad css del elemento html con el id encerrado en los primeros parentesis
		$(".box_head_reports_table_detail<?php echo $count; ?>").css("display","none");
		$(".box_result_table_reports_detail<?php echo $count; ?>").css("display","none");
	}
	
});
//fin del script
</script>
				<?php foreach ($row as $cell) { 
				
				
				
				
				?>
				
				<div class="casilla_result_report"><?php echo $cell; ?></div>
				<?php } ?>
			</div>
			
		</div>
			<div id="box-oculto-report">
				<div class="box_head_reports_table_detail<?php echo $count; ?>">
						<div class="file_report_head_detail">
							<?php foreach ($headers['details'] as $header) { ?>
							<div class="casilla_head_report_detail"><?php echo $header; ?></div>
							<?php } ?>
						</div>
					</div>
				
					<div class="box_result_table_reports_detail<?php echo $count; ?>">
						<?php foreach ($details_data[$key] as $row2) { ?>
						
							<div class="file_report_result_detail">
								<?php foreach ($row2 as $cell) { ?>
								<div class="casilla_result_report_detail"><?php echo $cell; ?></div>
								<?php } ?>
							</div>
						<?php } ?>
							</div>
				</div>
				
			
			<?php } ?>
		
		
	
	</div>
</div>





<div id="report_summary">
<?php foreach($overall_summary_data as $name=>$value) { ?>
	
<?php }?>
</div>
<?php 
if($export_excel == 1){
	$this->load->view("partial/footer_excel");
	$content = ob_end_flush();
	
	$filename = trim($filename);
	$filename = str_replace(array(' ', '/', '\\'), '', $title);
	$filename .= "_Export.xls";
	header('Content-type: application/ms-excel');
	header('Content-Disposition: attachment; filename='.$filename);
	echo $content;
	die();
	
}else{
	 
?>

<?php 
} // end if not is excel export 
?>

  
  
  
  

