<?php $this->load->view("partial/header"); ?>
<div id="page_title2" style="margin-bottom:8px;"><?php echo $this->lang->line('reports_reports'); ?></div>
<div id="contenido-report"><span id="welcome_message"><?php echo $this->lang->line('reports_welcome_message'); ?></span>

	<ul class="box-li-report">
		<h3><?php echo $this->lang->line('reports_summary_reports'); ?></h3>
		
			
			<li><a href="<?php echo site_url('reports/summary_suppliers');?>"><?php echo $this->lang->line('title_input_report_summary_suppliers'); ?></a></li>
			
			<li><a href="<?php echo site_url('reports/summary_receivings');?>"><?php echo $this->lang->line('title_input_report_summary_receivings'); ?></a></li>
			
			<li><a href="<?php echo site_url('reports/summary_customers');?>"><?php echo $this->lang->line('title_input_report_summary_customers'); ?></a></li>
			
			<li><a href="<?php echo site_url('reports/Summary_services');?>"><?php echo $this->lang->line('title_input_report_summary_prestamos'); ?></a></li>
			

	</ul>
	<ul class="box-li-report">
		<h3><?php echo $this->lang->line('reports_detailed_reports'); ?></h3>
		
			<li><a href="<?php echo site_url('reports/detailed_sales');?>"><?php echo $this->lang->line('reports_sales'); ?></a></li>
			<li><a href="<?php echo site_url('reports/detailed_receivings');?>"><?php echo $this->lang->line('reports_receivings'); ?></a></li>
			
			<li><a href="<?php echo site_url('reports/detailed_services');?>"><?php echo $this->lang->line('reports_services'); ?></a></li>
			
			<!--li><a href="<?php echo site_url('reports/specific_customer');?>"><?php echo $this->lang->line('reports_customer'); ?></a></li>
			<li><a href="<?php echo site_url('reports/specific_employee');?>"><?php echo $this->lang->line('reports_employee'); ?></a></li-->
		
	
		
	</ul>
	<ul class="box-li-report">
		<h3><?php echo $this->lang->line('reports_inventory_reports'); ?></h3>
		
			<li><a href="<?php echo site_url('reports/inventory_low');?>"><?php echo $this->lang->line('reports_low_inventory'); ?></a></li>
			<li><a href="<?php echo site_url('reports/inventory_summary');?>"><?php echo $this->lang->line('reports_inventory_summary'); ?></a></li>
		
		
	</ul>

</div>
<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}
?>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
});
</script>


</div>

</div>

<div id="box-footer">
<?php $this->load->view("partial/footer"); ?>

</div>
