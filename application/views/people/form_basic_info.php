<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('common_first_name').':', 'first_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'first_name',
		'id'=>'first_name',
		'value'=>$person_info->first_name)
	);?>
	</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('common_last_name').':', 'last_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'last_name',
		'id'=>'last_name',
		'value'=>$person_info->last_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">	

	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'email',
		'id'=>'email',
		'type'=>'hidden',
		'value'=>$person_info->email)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('common_phone_number').':', 'phone_number'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'phone_number',
		'id'=>'phone_number',
		'value'=>$person_info->phone_number));?>
	</div>
</div>





	<?php echo form_input(array(
		'name'=>'address_1',
		'id'=>'address_1',
		'type'=>'hidden',
		'value'=>$person_info->address_1));?>






	<?php echo form_input(array(
		'name'=>'address_2',
		'id'=>'address_2',
		'type'=>'hidden',
		'value'=>$person_info->address_2));?>
	





	<?php echo form_input(array(
		'name'=>'city',
		'id'=>'city',
		'type'=>'hidden',
		'value'=>$person_info->city));?>





	<?php echo form_input(array(
		'name'=>'state',
		'id'=>'state',
		'type'=>'hidden',
		'value'=>''));?>






	<?php echo form_input(array(
		'name'=>'zip',
		'id'=>'zip',
		'type'=>'hidden',
		'value'=>''));?>





	

	<?php echo form_input(array(
		'name'=>'country',
		'id'=>'country',
		'type'=>'hidden',
		'value'=>$person_info->country));?>




