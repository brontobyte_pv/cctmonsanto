<?php $this->load->view("partial/header"); ?>


<div id="page_title2"><?php echo $this->lang->line('recvs_register'); ?></div>

<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}
?>



<div id="register_wrapper">



	<?php $mode='receive';?>
	<?php echo form_open("receivings/add",array('id'=>'add_item_form')); ?>
	<label id="item_label" for="item">

	<?php
	if($mode=='receive')
	{
		echo $this->lang->line('recvs_find_or_scan_item');
	}
	else
	{
		echo $this->lang->line('recvs_find_or_scan_item_or_receipt');
	}
	?>
	</label>
<?php echo form_input(array('name'=>'item','id'=>'item','size'=>'40'));?>

<div id="cover_btn_new_art_entradas">
		<?php echo anchor("items/view/-1/width:360",
		"<div id='btn_new_art_entradas' class='btn_ok'><span>".$this->lang->line('sales_new_item')."</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line('sales_new_item')));
		?>
</div>
</form>

<!-- Receiving Items List -->

<section id="cover_register">

<div id="filas-head">
<div class="head-filas"><?php echo $this->lang->line('recvs_quantity'); ?></div>

<div class="head-filas" id="item-name_r"><?php echo $this->lang->line('recvs_item_name'); ?></div>

<div class="head-filas"><?php echo $this->lang->line('recvs_cost'); ?></div>
<div class="head-filas"><?php echo $this->lang->line('common_delete'); ?></div>


<div class="head-filas"><?php echo $this->lang->line('recvs_total'); ?></div>
<div class="head-filas"><?php echo $this->lang->line('recvs_edit'); ?></div>
</div>


<?php
if(count($cart)==0)
{
?>
<div class='warning_message' id='warning_message_reg'><?php echo $this->lang->line('sales_no_items_in_cart'); ?></div>
<?php
}
else
{
	foreach(array_reverse($cart, true) as $line=>$item)
	{
		echo form_open("receivings/edit_item/$line");
	?>
	
	
	
		<div id="filas-contenido">
		<div class="soloVisibleResponsivamente2">Cantidad</div>
		<div class="filas-items-reg">
		<?php
        	echo form_input(array('name'=>'quantity','value'=>$item['quantity'],'size'=>'2'));
		?>
		</div>

		<div class="soloVisibleResponsivamente2">Nombre del producto</div>
		<div  id="name-reg_r" class="filas-items-reg">
		<span><?php echo $item['name']; ?></span>

		
		<br />


		<?php if ($items_module_allowed)
		{
		?>
		</div>
		<div class="soloVisibleResponsivamente2">Costo</div>
		<div class="filas-items-reg"><?php echo form_input(array('name'=>'price','value'=>$item['price'],'size'=>'6'));?></div>
		<div class="soloVisibleResponsivamente2">Borrar</div>
		<div class="filas-items-reg"><?php echo anchor("receivings/delete_item/$line",'['.$this->lang->line('common_delete').']');?></div>
		<?php
		}
		else
		{
		?>
			<div class="soloVisibleResponsivamente2">Total</div>
			<div class="filas-items-reg"><?php echo $item['price']; ?></div>
			<?php echo form_input(array('name'=>'price','value'=>$item['price'],'type'=>'hidden')); ?>
		<?php
		}
		?>
		


		<div class="soloVisibleResponsivamente2">Total</div>
		<div class="filas-items-reg"><?php echo to_currency($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100); ?></div>
		
		<div class="soloVisibleResponsivamente2">Editar</div>
		<div class="filas-items-reg fila-edit-item"><?php echo form_submit("edit_item", $this->lang->line('sales_edit_item'));?></div>
		</div>
		
		
		</form>
		</form>
	<?php
	}
}
?>

</section>
</div>

<!-- Overall Receiving -->

<div id="overall_sale">
		<div class="cover_overall_process">
		<div id="box-delete-proverdor">
		<?php
		if(isset($supplier))
		{
			echo $this->lang->line("recvs_supplier").': <span><b>'.$supplier. '</b><span />';
			echo anchor("receivings/delete_supplier",'['.$this->lang->line('common_delete').' '.$this->lang->line('suppliers_supplier').']');?>
			
		</div>
			<div id="finish_sale">
		<?php echo form_open("receivings/complete",array('id'=>'finish_sale_form')); ?>
		
		<label id="comment_label" for="comment"><?php echo $this->lang->line('common_comments'); ?>:</label>
		<?php echo form_textarea(array('name'=>'comment','value'=>'','rows'=>'4','cols'=>'23'));?>
	
		
		<div id="box-pay">
		
		<span>
		<?php
		    echo form_input(array('name'=>'payment_type','type'=>'hidden'));?>
        
        </span>

        <span>
        
        <?php
			echo $this->lang->line('sales_amount_tendered').':   ';?>
		</span>
		<span>
		<?php
		    echo form_input(array('name'=>'amount_tendered','value'=>'','size'=>'10'));
		?>
      
        </span>

        </div>
		<?php echo "<div class='btn_ok' id='finish_sale_btn'><span>".$this->lang->line('recvs_complete_receiving')."</span></div>";
		?>
		
		
		</form>

	    <?php echo form_open("receivings/clear_receiving",array('id'=>'cancel_sale_form')); ?>
			    <div class='btn_ok' id='cancel_sale_btn'>
					<span>Cancel </span>
				</div>
        </form>
		</div>
		<?php
		}
		else
		{
			echo form_open("receivings/select_supplier",array('id'=>'select_supplier_form')); ?>
			<label class="overall_label" for="supplier"><?php echo $this->lang->line('recvs_select_supplier'); ?></label>
			<?php echo form_input(array('name'=>'supplier','id'=>'input_empieza','size'=>'30','value'=>$this->lang->line('recvs_start_typing_supplier_name')));?>
			</form>

		</div>
		
		<div class="cover_overall_process" id="box-or-btn">
		<h3 id="or"><?php echo $this->lang->line('common_or'); ?></h3>
		<?php echo anchor("suppliers/view/-1/width:350",
		"<div class='btn_ok' id='btn_new_proveedor_entradas'><span>".$this->lang->line('recvs_new_supplier')."</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line('recvs_new_supplier')));
		?>
		</div>
		<?php
	} 
	?>

	<div class="cover_overall_process" id='sale_details'>
		<div class="boxs-cover-sub-and-total"  id="boxs-cover-total" >
			<div class="price-total"><?php echo $this->lang->line('sales_total'); ?>:</div>
			<div class="price-total"  id="total_total" ><?php echo to_currency($total); ?></div>
		</div>
	</div>
	<?php
	if(count($cart) > 0)
	{
	?>
	

	</div>
	
	<?php
	}
	?>





<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $("#item").autocomplete('<?php echo site_url("receivings/item_search"); ?>',
    {
    	minChars:0,
    	max:100,
       	delay:10,
       	selectFirst: false,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#item").result(function(event, data, formatted)
    {
		$("#add_item_form").submit();
    });

	$('#item').focus();

	$('#item').blur(function()
    {
    	$(this).attr('value',"<?php echo $this->lang->line('sales_start_typing_item_name'); ?>");
    });

	$('#item,#input_empieza').click(function()
    {
    	$(this).attr('value','');
    });

    $("#input_empieza").autocomplete('<?php echo site_url("receivings/supplier_search"); ?>',
    {
    	minChars:0,
    	delay:10,
    	max:100,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#input_empieza").result(function(event, data, formatted)
    {
		$("#select_supplier_form").submit();
    });

    $('#input_empieza').blur(function()
    {
    	$(this).attr('value',"<?php echo $this->lang->line('recvs_start_typing_supplier_name'); ?>");
    });

    $("#finish_sale_btn").click(function()
    {
    	if (confirm('<?php echo $this->lang->line("recvs_confirm_finish_receiving"); ?>'))
    	{
    		$('#finish_sale_form').submit();
    	}
    });

    $("#cancel_sale_btn").click(function()
    {
    	if (confirm('<?php echo $this->lang->line("recvs_confirm_cancel_receiving"); ?>'))
    	{
    		$('#cancel_sale_form').submit();
    	}
    });


});

function post_item_form_submit(response)
{
	if(response.success)
	{
		$("#item").attr("value",response.item_id);
		$("#add_item_form").submit();
	}
}

function post_person_form_submit(response)
{
	if(response.success)
	{
		$("#input_empieza").attr("value",response.person_id);
		$("#select_supplier_form").submit();
	}
}

</script>
</div>
</div>
<div id="box-footer">
<?php $this->load->view("partial/footer"); ?>

</div>

