<?php
require_once ("secure_area.php");
//require_once ("interfaces/iprescription_controller.php");
class Pedidos extends Secure_area //implements iPrescription_controller
{
	function __construct()
	{
		parent::__construct('pedidos');
		
	}
	
	function index()
	{
		$config['base_url'] = site_url('?c=services&m=index');
		$config['total_rows'] = $this->Pedido->count_all();
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_pedidos_manage_table($this->Pedido->get_all($config['per_page'], $this->input->get('per_page')),$this);
		$this->load->view('pedidos/manage',$data);
        //$this->output->enable_profiler(TRUE);
		
	}
	function view($pedido_id=-1)
	{
		$data['info']=$this->Pedido->get_info($pedido_id);
		
		foreach($this->Customer->get_all()->result_array() as $row)
		{
			$clientes[$row['first_name'].' '. $row['last_name']] =$row['first_name'] .' '. $row['last_name'];
		}
		foreach($this->Employee->get_all()->result_array() as $row)
		{
			$empleados[$row['first_name'].' '. $row['last_name']] =$row['first_name'] .' '. $row['last_name'];
		}
		$estado=array(
                  '1'  => 'Pendiente',
                  '0'    =>'Cancelada');
		 $data['clientes']=$clientes;
		  $data['empleados']=$empleados;
		 $data['estado']=$estado;
		$this->load->view("pedidos/form",$data);
	}
	
	function get_form_width()
	{
		return 350;
	}
	function save()
	{	
		$cita_data = array(
		
		
		'fecha'=>$this->input->post('fecha'),
		'nom_cliente'=>$this->input->post('nom_cliente')
		);
		$this->Pedido->save($cita_data);
		
	}
	
	function suggest_pac()
	{
		$suggestions = $this->Pedido->get_pac_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}
	function cancel()
	{
		
        //$this->output->enable_profiler(TRUE);
		$pedido_id=$this->input->post('ids');
		$cur_app_info = $this->Pedido->get_info($appointment_id);
		if($this->Appointment->cancelar($appointment_id))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_updating').' '.
			$cur_app_info->pedido_id,'pedido_id'=>$pedido_id));
			
		}else
		{
			
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('items_error_adding_updating').' '.
			$cur_app_info->pedido_id,'pedido_id'=>-1));
			
			
		}
		
		
	}
	function get_row()
	{
		$appointment_id = $this->input->post('row_id');
		$data_row=get_appointments_data_row($this->Pedido->get_info($pedido_id),$this);
		echo $data_row;
	}
	
	
	
	
}