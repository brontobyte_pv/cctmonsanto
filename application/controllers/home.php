<?php
require_once ("secure_area.php");
class Home extends Secure_area 
{
	function __construct()
	{
		parent::__construct();	
		
	}
	
	function index()
	{
		$data['controller_name']=strtolower(get_class());
		$this->load->view("home",$data);
	}
	
	function logout()
	{
		
		
		$this->Employee->logout();
		
		
		
	}
	
	function backup()
	{
		
		$this->load->dbutil();
		
			$año=date('Y');	
			$mes=date('F');	
			$dia=date('j');	
			
			if(!file_exists('backups/'.$año))
			{
				mkdir('backups/'.$año,0777);
			}
			
			if(!file_exists('backups/'.$año.'/'.$mes))
			{
				mkdir('backups/'.$año.'/'.$mes,0777);	
			}
			
			if(!file_exists('backups/'.$año.'/'.$mes.'/'.$dia))
			{
				mkdir('backups/'.$año.'/'.$mes.'/'.$dia,0777);
			}
			
				
		$prefs = array(
                'tables'      => array(),  // Aqui agregamos todas las tablas
                'ignore'      => array(),           //Se puede excluir alguna por lo pronto ninguna
                'format'      => 'txt',             // gzip, zip, txt
                'filename'    => 'respaldo_msp.sql',    // Nombre del archivo 
                'add_drop'    => TRUE,              //Crea una copia de la estructura
                'add_insert'  => TRUE,              // Crea una sentencia de insertar con los datos de las tablas para futuras restauraciones
                'newline'     => "\n"               // Caracater de salto de linea
              );
		$backup =& $this->dbutil->backup($prefs); 
		
		
		$this->load->helper('file');
		write_file('backups/'.$año.'/'.$mes.'/'.$dia.'/respaldo_msp.sql', $backup); 
		
		$this->load->helper('download');
		
		force_download('respaldo_msp.sql', $backup);
		
	}
}
?>