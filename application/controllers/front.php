<?php
class Front extends Controller
{
	function __construct()
	{
		parent::__construct();	
		$this->load->library('pedido_lib');
		$this->load->library('lang_lib');
	}
	
	function index()
	{
		$person_data = array(
		'mensaje'=>$this->input->post('email'));
		$data['error_captcha']='';
	
		
		//////////Captcha
		$this->form_validation->set_rules('mensaje', "Name", 'required');
		$word = $this->session->userdata('captchaWord');
		$palabra=rand(0, 1000);
		if ($this->form_validation->run() == TRUE && strcmp(strtoupper($person_data['captcha']),strtoupper($word)) == 0)
    {
      /** Validation was successful; show the Success view **/
      
      
      /* Clear the session variable */
      $this->session->unset_userdata('captchaWord');
      
     $this->mail_contact_msj($person_data);
      /* Get the user's name from the form */
      $name = set_value('name');
	  $correo = set_value('correo');
		$data['error_captcha']='';
     $data = array('name' => $name,'correo' => $correo);
      $this->load->view('contact_msj', $data);}
	  else{ $vals = array(
		'word' => $palabra,
		'img_path' => './images/',
        'img_url' => base_url() .'images/',
		'img_width' => '100',
		'expiration' => 3600);
		$captcha = create_captcha($vals);
		$data['img']=$captcha['image'];
		if ((!empty($person_data['captcha'])&&!empty($person_data['name']))||(empty($person_data['captcha'])&& !empty($person_data['name'])))
		{
		$data['error_captcha']='<span id="error_captcha">Error captcha</span>';
		}else{$data['error_captcha']='';}
		$this->session->set_userdata('captchaWord', $captcha['word']);
		
		//Aqui cominezan el cambio de idioma
		$idioma=$this->lang_lib->get_idioma();
		if(empty($idioma)){$idioma='english';}	
		$data['link']= '';
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$this->load->view("front",$data);
	}
	}
	
	function servicios()
	{
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$this->load->view("servicios",$data);
	}
	function contacto(){
		$datos_del_form = array(            //array que obtiene los valores del view 
		'asunto'=>$this->input->post('asunto'),
		'nombre'=>$this->input->post('nombre'),
		'correo'=>$this->input->post('correo'),
		'mensaje'=>$this->input->post('mensaje'),
		'captcha'=>$this->input->post('captcha'));
		
		$this->form_validation->set_rules('nombre','Nombre','required');//pone la regla de validacion para los data 
		$this->form_validation->set_rules('correo','Correo','required');
		$this->form_validation->set_rules('asunto','Asunto','required');
		$this->form_validation->set_rules('mensaje','Mensaje','required');
		$word = $this->session->userdata('captchaWord');
		$data['error_msj']='';
		
	if ($this->form_validation->run() == TRUE && strcmp(strtoupper($datos_del_form['captcha']),strtoupper($word)) == 0)//valida los data dependiendo de las reglas establecidas anteriormente con la funcion set_rules
		{
			 
			$this->mail_contact_msj($datos_del_form);
		}else{
			//codigo que se ejecuta si la validacion falla
			 //Captcha
		 
		$palabra=rand(0, 1000);
		$vals = array(
		'word' => $palabra,
		'img_path' => './images/',
        'img_url' => base_url() .'images/',
		'img_width' => '100',
		'expiration' => 3600);
		$captcha = create_captcha($vals);
		$data['img']=$captcha['image'];
	
		$this->session->set_userdata('captchaWord', $captcha['word']);
		
			$data['error_msj']='Error de captcha';
			
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$this->load->view("contacto",$data);
	}
	}
	
	function portafolio()
	{
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$this->load->view("portafolio",$data);
	}
	
	function mail_contact_msj($to_send, $action) {
		/*if($action==1){
			$asunto=$to_send['asunto'];
			$cuerpo='Una persona de nombre: '.$to_send['nombre'].' <br>Quiere hacer una reservacion el dia<br>"'.$to_send['date'].'"<br>A las :'.$to_send['time'].'  para: '.$to_send['nu_personas'].' personas <br> Comentarios: '.$to_send['mensaje'].'';		
	/*	echo 'Una persona de nombre: <b>'.$to_send['nombre'].'</b><br>Quiere hacer una reservacion el dia<br>"'.$to_send['date'].'"<br>A las :'.$to_send['time'].'  para: '.$to_send['nu_personas'].' personas <br> Comentarios: '.$to_send['mensaje'].'';
		}*/
	//	if($action==2){
			$asunto=$to_send['asunto'];
			$cuerpo='Una persona de nombre: '.$to_send['nombre'].'<br>Nos envia el siguiente mensaje:<br>'.$to_send['mensaje'].' ';
		/*	echo 'Una persona de nombre: '.$to_send['name'].'<br>Nos envia el siguiente mensaje:<br>"'.$to_send['mensaje'].'" ';*/
		/*}*/
		
		$this->load->library('email');
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from($to_send['correo'],'Mensaje de la WEB');
		$this->email->to('info@puertovallartadiscovery.com, reservacion@puertovallartadiscovery.com'); 
		$this->email->subject($asunto);
		$this->email->message($cuerpo);			
		$this->email->send();
	}
	
}
?>