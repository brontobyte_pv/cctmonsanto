<?php
require_once ("secure_area.php");
//require_once ("interfaces/iprescription_controller.php");
class Gastos extends Secure_area //implements iPrescription_controller
{
	function __construct()
	{
		parent::__construct('gastos');
		
	}
	
	function index()
	{
		$config['base_url'] = site_url('?c=services&m=index');
		$config['total_rows'] = $this->Gasto->count_all();
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		foreach($this->Recurso->get_all()->result_array() as $row)
		{
		$recursos[$row['recurso_id']] =$row['nombre'];
		}
		foreach($this->Recurso->get_all()->result_array() as $row)
		{
		$cuentas[$row['recurso_id']] ='  '.$row['nombre'].'  '.$row['recurso'];
		}
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_gastos_manage_table($this->Gasto->get_all($config['per_page'], $this->input->get('per_page')),$this);
		 if(empty($cuentas))
				  {
					  $cuentas['-1'] ='No existen';
				  }
		$data['cuentas']=$cuentas;
		$this->load->view('gastos/manage',$data);
        //$this->output->enable_profiler(TRUE);
		
	}
	function view($gasto_id=-1)
	{
		$data['info']=$this->Gasto->get_info($gasto_id);
		
		/*foreach($this->Customer->get_all()->re|sult_array() as $row)
		{
			$clientes[$row['first_name'].' '. $row['last_name']] =$row['first_name'] .' '. $row['last_name'];
		}*/
		foreach($this->Recurso->get_all()->result_array() as $row)
		{
		$recursos[$row['recurso_id']] =$row['nombre'];
		}
		$estado=array(
                  'Pendiente'  => 'Pendiente',
                  'Hecho'    =>'Hecho');
		$modo=array(
                  'Efectivo'  => 'Efectivo',
                  'Cheque'    =>'Cheque',
				  'Transferencia' =>'Transferencia',
				  'Tarjeta'    =>'Tarjeta');
				  if(empty($recursos))
				  {
					  $recursos['-1'] ='No existen';
				  }
		$data['recursos']=$recursos;
		$data['modo']=$modo;
		$data['estado']=$estado;
		$this->load->view("gastos/form",$data);
	}
	
	function get_form_width()
	{
		return 700;
	}
	function save($gasto_id=-1)
	{	
		$gastos_data = array(
		'fecha'=>$this->input->post('fecha'),
		'concepto'=>$this->input->post('concepto'),
		'recurso_id'=>$this->input->post('recurso'),
		'estado'=>$this->input->post('estado'),
		'cantidad'=>$this->input->post('cantidad'),
		'tipo'=>$this->input->post('modo')
		);
		if($this->Gasto->save($gastos_data,$gasto_id))
		{
			if($gastos_data['estado']=='Hecho')
			{
				$this->Recurso->rest_recurso($gastos_data['cantidad'],$gastos_data['recurso_id']);
			}	
			if($gasto_id==-1)
			{
				
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('item_kits_successful_adding')));
				
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('item_kits_successful_updating')));
			}
		}
			
		else//failure
		{
				echo json_encode(array('error'=>true,'message'=>$this->lang->line('item_kits_error_adding_updating')));
		}
			
		
		
	}
	
	
	function cancel()
	{
		
        //$this->output->enable_profiler(TRUE);
		$gasto_id=$this->input->post('ids');
		$cur_app_info = $this->Gasto->get_info($appointment_id);
		if($this->Appointment->cancelar($appointment_id))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_updating').' '.
			$cur_app_info->gasto_id,'gasto_id'=>$gasto_id));
			
		}else
		{
			
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('items_error_adding_updating').' '.
			$cur_app_info->gasto_id,'gasto_id'=>-1));
			
			
		}
		
		
	}
	function get_row()
	{
		$appointment_id = $this->input->post('row_id');
		$data_row=get_gastos_data_row($this->Gasto->get_info($gasto_id),$this);
		echo $data_row;
	}
	
	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_gastos_manage_table_data_rows($this->Gasto->search($search),$this);
		echo $data_rows;
	}
	
	function suggest()
	{
	$suggestions = $this->Gasto->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	
	
}