<?php
require_once ("secure_area.php");
//require_once ("interfaces/iprescription_controller.php");
class Services extends Secure_area //implements iPrescription_controller
{
	function __construct()
	{
		parent::__construct('services');
		$this->load->library('servicios_lib');
	}
	
	function index()
	{
		$config['base_url'] = site_url('?c=services&m=index');
		$config['total_rows'] = $this->Service->count_all();
		$config['per_page'] = '15'; 
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_services_manage_table($this->Service->get_all($config['per_page'], $this->input->get('per_page')),$this);
		$customer_id=$this->servicios_lib->get_customer();
		if($customer_id!=-1)
		{
			$info=$this->Customer->get_info($customer_id);
			$data['customer']=$info->first_name." ".$info->last_name;
			
		}
		$this->load->view('services/manage',$data);
		$this->output->enable_profiler(TRUE);
		
	}
		
	function view_edit($service_id)
	{
		$data['info']=$this->Service->get_info($service_id);
		
		$empleado=$this->Employee->get_logged_in_employee_info()->first_name.' '.$this->Employee->get_logged_in_employee_info()->last_name;
		
		$estado=array(
                  'Pendiente'  => 'Pendiente',
                  'Entregado'    =>'Entregado');
		
		
		
		$data['nom_asignado']=$empleado;
		$data['estado']=$estado;
		
		
		
			$data['items_info']=$this->Service->get_info($service_id);
			
			$data['customer']=$data['info']->nom_cliente;
			$this->load->view("services/form",$data);
			
		
		
	}
	
	function view($service_id=-1)
	{
		$data['info']=$this->Service->get_info($service_id);
		
		$empleado=$this->Employee->get_logged_in_employee_info()->first_name.' '.$this->Employee->get_logged_in_employee_info()->last_name;
		
		$estado=array(
                  'Pendiente'  => 'Pendiente',
                  'Entregado'    =>'Entregado');
		
		
		
		$data['nom_asignado']=$empleado;
		$data['estado']=$estado;
		$customer_id=$this->servicios_lib->get_customer();
		if($customer_id!=-1)
		{
			$data['items_info']=$this->Service->get_info($service_id);
			$info=$this->Customer->get_info($customer_id);
			$data['customer']=$info->first_name." ".$info->last_name;
			$this->load->view("services/form",$data);
			
		}
		else
		{
			$this->load->view("services/form_error",$data);
		}
	}
	
	function get_form_width()
	{
		return 500;
	}
	function save($service_id=-1)
	{	
		
			
		
		$service_data = array(
		
		'nom_cliente'=>$this->input->post('nom_cliente'),
		'motivo'=>$this->input->post('motivo'),
		'fecha'=>date('Y-m-d H:i:s'),
		'nom_asignado'=>$this->input->post('nom_asignado'),
		'accion'=>$this->input->post('accion'),
		'reporte'=>$this->input->post('reporte'),
		'estado'=>$this->input->post('estado'),
		'duracion'=>$this->input->post('duracion')
		
		);
		$productos_prestamo=$this->input->post("productos_prestamo")!=false ? $this->input->post("productos_prestamo"):array();
		
		if($service_id==-1)
		{
			$service_id=$this->Service->save($service_data,$service_id);
			$this->Item_kit_items->save($productos_prestamo, $service_id);
			json_encode(array('success'=>true,'message'=>$this->lang->line('item_kits_successful_adding')));
			
			
			
			
		}
		elseif($service_id!=-1)
		{
			$this->Service->save($service_data,$service_id);
			$this->Item_kit_items->save($productos_prestamo, $service_id);
			json_encode(array('success'=>false,'message'=>$this->lang->line('item_kits_successful_updating')));
			
			
			
		}
		else
		{
			json_encode(array('error'=>true,'message'=>$this->lang->line('item_kits_error_adding_updating')));
			
		}
		$this->remove_customer();
		//if($services_data=$this->Service->save($service_data,$service_id))
		//{
			//New item kit
			
		//	{
				
				
				//echo json_encode(array('success'=>true,'message'=>$this->lang->line('item_kits_successful_adding')));
				
		//	}
		//	else //previous item
		//	{
				
			//	$this->Item_kit_items->save($productos_prestamo, $service_id);
				//echo json_encode(array('success'=>true,'message'=>$this->lang->line('item_kits_successful_updating')));
				
			//}
		//}
			
		//else//failure
			//{
				//echo json_encode(array('error'=>true,'message'=>$this->lang->line('item_kits_error_adding_updating')));
			//}
		
		//$this->output->enable_profiler(TRUE);
	}
	
	
	function delete()
	{
		
		
		$services_to_delete=$this->input->post('ids');
		
		if($this->Service->delete_list($services_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('item_kits_successful_deleted').' '.
			count($services_to_delete).' '.$this->lang->line('item_kits_one_or_multiple')));
			
		}
		else
		{
			
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('item_kits_cannot_be_deleted')));
			
			
		}
		
		
	}
	
	function get_row()
	{
		$service_id = $this->input->post('row_id');
		$data_row=get_services_data_row($this->Service->get_info($service_id),$this);
		echo $data_row;
	}
	
	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_services_manage_table_data_rows($this->Service->search($search),$this);
		echo $data_rows;
	}
	
	function suggest()
	{
		$suggestions = $this->Service->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	
	function customer_search()
	{
		$suggestions = $this->Customer->get_customer_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	
	function select_customer()
	{	
		$customer_id = $this->input->post("customer");
		$this->servicios_lib->set_customer($customer_id);
		
		$this->index();
		
	}
	
	function remove_customer()
	{
		$this->servicios_lib->remove_customer();
		
		$this->index();
	}

	
	
	
}