<?php
class Catalogo extends Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('pedido_lib');
	}
	
	function index()
	{
		$categoria = $this->uri->segment(3);
		$config['base_url'] = site_url('?c=items&m=index');
		$config['total_rows'] = $this->Item->count_all_cat();
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		$data['categoria']=$categoria;
		$data['controller_name']=strtolower(get_class());
		$data['manage_table']=get_itemsCAT_manage_table($this->Item->get_categories($config['per_page'], $this->input->get('per_page')),$this);
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$this->load->view('catalogo',$data);
	}
	
	
	
	function items_data($categoria)
	{
		
	
	
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$config['base_url'] = site_url('?c=items&m=index');
		$config['total_rows'] = $this->Item->count_all_cat();
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		$data['categoria']=$categoria;
		$data['controller_name']=strtolower(get_class());
		$data['manage_categorie_aside']=get_categoria_manage_table($this->Item->get_categories($config['per_page'], 
		$this->input->get('per_page')),$this);
		$data['manage_Catitems']=get_CatItems_manage_table($this->Item->get_itemsCAT($categoria,$config['per_page'], $this->input->get('per_page')),$this);
		$this->load->view('lista-categoria',$data);
	
	
	
	
	
	}
	
	function detalles($item_id)
	{	
		$data['cart']=$this->pedido_lib->get_cart($item_id);
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$data['item_agregado']='';
		if($this->pedido_lib->item_in_cart($item_id))
		{
			
				$data['item_agregado']='Este Árticulo ya fue agregado al pedido.Puedes ordenar mas';
			
		}
		$datos_item=$this->Item->get_info($item_id);
		
		$i=0;
		foreach($datos_item as $categoria)
		{
				
				
				$datos_pro[$i]=$categoria;
				$i++;
				
		}
		$data['categoria']=$datos_pro[1];
		$data['cantidad']=$datos_pro[7];
		$data['item_nom']=$datos_pro[0];
		$data['precio']=$datos_pro[6];
		$data['descripcion']=$datos_pro[4];
		$data['id']=$datos_pro[11];
		$data['marca']=$datos_pro[17];
		$data['modelo']=$datos_pro[18];
		$config['base_url'] = site_url('?c=items&m=index');
		
		$data['item_info']=$this->Item->get_info($item_id);
		$this->load->view("detalle-item",$data);
	} 
	function get_form_width()
	{
		return 360;
	}
	
}