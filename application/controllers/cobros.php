<?php
require_once ("secure_area.php");
//require_once ("interfaces/iprescription_controller.php");
class Cobros extends Secure_area //implements iPrescription_controller
{
	function __construct()
	{
		parent::__construct('cobros');
		$this->load->library('cobros_lib');
	}
	
	function index()
	{
		$config['base_url'] = site_url('?c=services&m=index');
		$config['total_rows'] = $this->Cobro->count_all();
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_cobros_manage_table($this->Cobro->get_all($config['per_page'], $this->input->get('per_page')),$this);
		$customer_id=$this->cobros_lib->get_customer();
		if($customer_id!=-1)
		{
			$info=$this->Customer->get_info($customer_id);
			$data['customer']=$info->first_name." ".$info->last_name;
			
		}
		$this->load->view('cobros/manage',$data);
        //$this->output->enable_profiler(TRUE);
		
	}
	function view($cobro_id=-1)
	{
		$data['info']=$this->Cobro->get_info($cobro_id);
		foreach($this->Recurso->get_all()->result_array() as $row)
		{
		$recursos[$row['recurso_id']] =$row['nombre'];
		}
		
		
		$estado=array(
                  'Pendiente'  => 'Pendiente',
                  'Hecho'    =>'Hecho');
		
		$modo=array(
                  'Efectivo'  => 'Efectivo',
                  'Cheque'    =>'Cheque',
				  'Transferencia' =>'Transferencia',
				  'Tarjeta'    =>'Tarjeta');
		
		$data['recursos']=$recursos;
		$data['modo']=$modo;
		$data['estado']=$estado;
		
		$customer_id=$this->cobros_lib->get_customer();
		if($customer_id!=-1)
		{
			
			$info=$this->Customer->get_info($customer_id);
			$query=$this->Sale->cuentas_pendientes($customer_id);
			
			if(count($query)!=0)
			{
				foreach($this->Sale->cuentas_pendientes($customer_id)->result_array() as $row)
			{
				$cuentas_abiertas[$row['sale_id']] ="Folio:".$row['sale_id']." Cantidad Abonada:".$row['payment_amount'];
			}
				
			
			}
			else
			{
				$cuentas_abiertas=array(
                  '-1'  => 'No hay');
			}
			
			
			$data['cuentas_abiertas']=$cuentas_abiertas;
			$data['customer']=$info->first_name." ".$info->last_name;
			$this->load->view("cobros/form",$data);
			
			
		}
		else
		{
			$this->load->view("cobros/form_error",$data);
		}
		$this->output->enable_profiler(TRUE);
	}
	
	function get_form_width()
	{
		return 700;
	}
	function save($cobro_id=-1)
	{	
		$cobros_data = array(
		'fecha'=>$this->input->post('fecha'),
		'nom_cliente'=>$this->input->post('nom_cliente'),
		'concepto'=>$this->input->post('concepto'),
		'estado'=>$this->input->post('estado'),
		'cantidad'=>$this->input->post('cantidad'),
		'tipo'=>$this->input->post('modo'),
		'recurso_id'=>$this->input->post('recurso'),
		'sale_id'=>$this->input->post('cuentas_abierta')
		
		);
		if($this->Cobro->save($cobros_data,$cobro_id))
		{
			if($cobros_data['estado']=='Hecho')
			{
				$this->Recurso->sum_recurso($cobros_data['cantidad'],$cobros_data['recurso_id']);
				$this->Sale->update_payment_amount($cobros_data['cantidad'],$cobros_data['sale_id']);
				
			}
			if($cobro_id==-1)
			{
				
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('item_kits_successful_adding')));
				
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('item_kits_successful_updating')));
			}
		
		}
		else//failure
		{
			echo json_encode(array('error'=>true,'message'=>$this->lang->line('item_kits_error_adding_updating')));
		}
		
	}
	
	function suggest()
	{
		$suggestions = $this->Cobro->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	
	function cancel()
	{
		
        //$this->output->enable_profiler(TRUE);
		$cobro_id=$this->input->post('ids');
		$cur_app_info = $this->Cobro->get_info($appointment_id);
		if($this->Appointment->cancelar($appointment_id))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_updating').' '.
			$cur_app_info->cobro_id,'cobro_id'=>$cobro_id));
			
		}else
		{
			
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('items_error_adding_updating').' '.
			$cur_app_info->cobro_id,'cobro_id'=>-1));
			
			
		}
		
		
	}
	function get_row()
	{
		$cobro_id = $this->input->post('row_id');
		$data_row=get_cobros_data_row($this->Cobro->get_info($cobro_id),$this);
		echo $data_row;
	}
	
	function customer_search()
	{
		$suggestions = $this->Customer->get_customer_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	
	function select_customer()
	{	
		$customer_id = $this->input->post("customer");
		$this->cobros_lib->set_customer($customer_id);
		
		$this->index();
		
	}
	
	function remove_customer()
	{
		$this->cobros_lib->remove_customer();
		
		$this->index();
	}
	
}