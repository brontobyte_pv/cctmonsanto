<?php

class Cart extends Controller
{
	function __construct()
	{
		parent::__construct();	
		$this->load->library('pedido_lib');
	}
	
	function index()
	{
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$data['total']=$this->pedido_lib->get_total();
		$this->load->view("pedido",$data);
		
		
		
	}
	function add()
	{
		if($this->Customer->is_logged_in())
		{
		$data=array();
		$item_id = $this->uri->segment(3);
		$quantite=null;
		
		$exist=$this->pedido_lib->add_item($item_id);
		$data['cart']=$this->pedido_lib->get_cart();
		
		$data['total']=$this->pedido_lib->get_total();
		if(!$exist){
		{$data['error']=$this->lang->line('sales_unable_to_add_item');}
		}
		$this->_reload($data);
		redirect('/catalogo/detalles/'.$item_id, 'refresh');
		
		}else
		{
			
			
			redirect('registro');
			
			
		}
	}
	function _reload($data=array())
	{
		
		
		$person_info = $this->Customer->get_logged_in_customer_info();
		$data['cart']=$this->pedido_lib->get_cart();
		$data['total']=$this->pedido_lib->get_total();
	
		
		

		
		if(!$this->Customer->is_logged_in())
		{
			$info=$this->Customer->get_info($customer_id);
			$data['customer']=$info->first_name." ".$info->last_name;
			$data['customer_email']=$info->email;
		}
		
		
		
	}
	function cancelar_pedido()
    {
    	$this->pedido_lib->empty_cart();

    	redirect('/cart/', 'refresh');

    }
	
	function borrar_item($line)
    {
		
    	
		$this->pedido_lib->delete_item($line);

    	redirect('/cart/', 'refresh');

    }
	
}
?>