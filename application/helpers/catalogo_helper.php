<?php
	function get_categoria_manage_table($categories,$controller)
	{
		$CI =& get_instance();
		$table='
		<div class="box-table-aside">
	<aside id="aside-catego">
		
		<h4>CATEGORIAS</h4>
	
	';
	
	$headers = array($CI->lang->line('items_category')
	
	);
	
	
	$table.=get_categorias_manage_table_data_rows($categories,$controller);
	$table.='</ul>
		
			</aside>
			</div>';
	return $table;
		
	}
function get_categorias_manage_table_data_rows($categories,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($categories->result() as $categorie)
	{
		$table_data_rows.=get_categorias_data_row($categorie,$controller);
	}
	
	if($categories->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('items_no_items_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}	
	/************************/
function get_categorias_data_row($categorie,$controller)
{
	$CI =& get_instance();
	
	

	$controller_name=strtolower(get_class($CI));
	

	$table_data_row='';
	

	$table_data_row.='<a  itemprop="url"  href="'.base_url().'index.php/catalogo/items_data/'.$categorie->category.'">'.$categorie->category.'</a>';
	

	
	$table_data_row.='';
	return $table_data_row;
}


/*******El catalogo muestra solo las categorias***********/
	function get_itemsCAT_manage_table($categories,$controller)
{
	$CI =& get_instance();
	$table='<section class="contenido" id="contenido-medio">
			       <div id="myTabContent" class="tab-content">
				   				       <div class="tab-pane fade in active" id="tab1">';
	

	
	
	
	$table.=get_itemsCAT_manage_table_data_rows($categories,$controller);
	$table.=' </div>
		</div>
		</section>';
	return $table;
}
	
	function get_itemsCAT_manage_table_data_rows($categories,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($categories->result() as $categorie)
	{
		$table_data_rows.=get_itemCAT_data_row($categorie,$controller);
	}
	
	if($categories->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('items_no_items_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}
	
	function get_itemCAT_data_row($categorie,$controller)
{
	$CI =& get_instance();
	
	$controller_name=strtolower(get_class($CI));
	$width = 200;

	$table_data_row='<article class="box-art-categorias" itemprop="category" itemscope itemtype="http://schema.org/Offer">';
	
	$table_data_row.='<div class="box-circle-catego">
					<div class="box-circle-in">
						<a  itemprop="url" href="'.base_url().'index.php/catalogo/items_data/'.$categorie->category.'"><img categorieprop="image" src="'.base_url().'images/img-catalago/'.$categorie->category.'/1.png" alt="'.$categorie->category.'" /></a>
					</div>
					</div>
				<a  categorieprop="url" class="boton-categorias" href="'.base_url().'index.php/catalogo/items_data/'.$categorie->category.'">'.$categorie->category.'</a>';
									
	

	$table_data_row.='</article>';
	return $table_data_row;
}
	
/**********/
function get_CatItems_manage_table($items,$controller)
	{
		$CI =& get_instance();
		$table='
<div itemscope itemtype="http://schema.org/Store" id="seccion-table-categoria" >
	
	';
	
	
	
	
	$table.=get_CatItems_manage_table_data_rows($items,$controller);
	$table.='
		
			</div>';
	return $table;
		
	}
	function get_CatItems_manage_table_data_rows($items,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($items->result() as $item)
	{
		$table_data_rows.=get_CatItems_data_row($item,$controller);
	}
	
	if($items->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('items_no_items_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}
function get_CatItems_data_row($item,$controller)
{
	$CI =& get_instance();
	
	$controller_name=strtolower(get_class($CI));
	$width = 200;

	$table_data_row='<article class="articles-categoria" itemprop="category" itemscope itemtype="http://schema.org/Offer">';
	
	$table_data_row.='<div class="img-box">
						<img itemprop="image" src="'.base_url().'images/items/'.$item->item_id.'/1.png"/>
					</div>
				<div class="txt-item">
				<span class="nombre">'.$item->name.'</span><span class="nombre">'.$item->brand.'</span><br/>
			<span class="precio">Precio: $'.$item->unit_price.'</span>
			</div>
			
		  	<a  itemprop="url" class="btn-mas2" href="'.base_url().'index.php/catalogo/detalles/'.$item->item_id.'">
					<span class="fx-btn2">
					 </span>
					 <img class="ico-btn2" src="'.base_url().'images/ico/point.png"/>
				<span class="txt-btn-mas2">Más info</span>
			
			</a>';
									
	

	$table_data_row.='</article>';
	return $table_data_row;
}
	

?>