<?php
class Pedido extends Model
{
	
	
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('pedidos');
		
		$this->db->order_by("fecha", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	
	function count_all()
	{
		$this->db->from('pedidos');
		return $this->db->count_all_results();
	}
	function get_info($pedido_id)
	{
		$this->db->from('pedidos');	
		$this->db->where('pedido_id',$pedido_id);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('gastos');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	function get_pac_suggestions($search)
	{
		$suggestions = array();
		
		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0");
		$this->db->order_by("last_name", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->first_name.' '.$row->last_name;		
		}

		return $suggestions;
		}
		
		function save($data)
		{
	
				$this->db->insert('pedidos',$data);
				
		}
		 function cancelar($pedido_id)
		 {
			 
			$this->db->where('pedido_id', $pedido_id);
			return $this->db->update('pedidos', array('estado' => 1)); 
			 
			 
		 }
}

?>