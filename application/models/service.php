<?php
class Service extends Model
{
	
	function exists($service_id)
	{
		$this->db->from('services');
		$this->db->where('service_id',$service_id);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('services');
		$this->db->where('estado', 'Pendiente');
		$this->db->order_by("fecha", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
		/*function get_all_repors($limit=10000, $offset=0)
	{
		$this->db->from('services');
		//$this->db->where('estado', 'Pendiente');
		$this->db->order_by("fecha", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}*/
	
	
	function count_all()
	{
		$this->db->from('services');
		return $this->db->count_all_results();
	}
	function get_info($service_id)
	{
		$this->db->from('services');	
		$this->db->where('service_id',$service_id);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('services');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	function search($search)
	{
		$this->db->from('services');
		$this->db->where("nom_cliente LIKE '%".$this->db->escape_like_str($search)."%' or 
		fecha LIKE '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by("service_id", "asc");
		return $this->db->get();	
	}
	
		
	function save(&$data,$service_id=false)
	{
				if(!$service_id or !$this->exists($service_id))
				{
					if($this->db->insert('services',$data))
					{
						$service_id=$this->db->insert_id();
						return $service_id;
					}
					
				}
				
			
				$this->db->where('service_id', $service_id);
				return $this->db->update('services',$data);
		
	}
	function delete_list($services_to_delete)
	{
			 
		$this->db->where_in('service_id',$services_to_delete);
		return $this->db->delete('services');
			
			 
			 
	}
		 
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('services');
		$this->db->like('nom_cliente', $search);
		$this->db->order_by("nom_cliente", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->nom_cliente;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
	public function create_services_temp_table()
	{
		$this->db->query("CREATE TEMPORARY TABLE ".$this->db->dbprefix('services_temp')."
		(SELECT ". 
		$this->db->dbprefix('item_kit_items').".service_id,".
		$this->db->dbprefix('item_kit_items').".item_id,".
		$this->db->dbprefix('item_kit_items').".quantity as quantity_prestado,
		".$this->db->dbprefix('items').".
		quantity,name,cost_price
		FROM ".$this->db->dbprefix('item_kit_items')."
		INNER JOIN ".$this->db->dbprefix('services')." ON  ".$this->db->dbprefix('item_kit_items').'.service_id='.$this->db->dbprefix('services').'.service_id'."
		INNER JOIN ".$this->db->dbprefix('items')." ON  ".$this->db->dbprefix('item_kit_items').'.item_id='.$this->db->dbprefix('items').'.item_id'."
		GROUP BY service_id, item_id)");
		
		$this->db->where('estado', 'Pendiente');
	}
}

?>