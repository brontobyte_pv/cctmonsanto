<?php
class Gasto extends Model
{
	
	function exists($gasto_id)
	{
		$this->db->from('gastos');
		$this->db->where('gasto_id',$gasto_id);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('gastos');
		
		$this->db->order_by("fecha", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	
	function count_all()
	{
		$this->db->from('gastos');
		return $this->db->count_all_results();
	}
	
	function get_info($gasto_id)
	{
		$this->db->from('gastos');	
		$this->db->where('gasto_id',$gasto_id);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('gastos');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	
		
	function save(&$gastos_data,$gasto_id=false)
	{
		if(!$gasto_id or !$this->exists($gasto_id))
		{
			if($this->db->insert('gastos',$gastos_data))
			{
				$gastos_data['gasto_id']=$this->db->insert_id();
				return true;
			}
		}
		$this->db->where('gasto_id', $gasto_id);
		return $this->db->update('gastos',$gastos_data);	
	}
	
	function cancelar($gasto_id)
	{
			 
		$this->db->where('gasto_id', $gasto_id);
		return $this->db->update('gastos', array('estado' => 1)); 
			 
			 
	}
	
	function search($search)
	{
		$this->db->from('gastos');
		$this->db->where("fecha LIKE '%".$this->db->escape_like_str($search)."%' or 
		estado LIKE '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by("fecha", "asc");
		return $this->db->get();	
	}
	
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('gastos');
		$this->db->like('estado', $search);
		$this->db->order_by("estado", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->estado;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

?>