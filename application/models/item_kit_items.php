<?php
class Item_kit_items extends Model
{
	/*
	Gets item kit items for a particular item kit
	*/
	function get_info($service_id)
	{
		$this->db->from('item_kit_items');
		$this->db->where('service_id',$service_id);
		//return an array of item kit items for an item
		return $this->db->get()->result_array();
		
	}

	/*
	Inserts or updates an item kit's items
	*/
	function save(&$item_kit_items, $service_id)
	{
		$success=false;
		$this->db->trans_start();
		$this->db->from('item_kit_items');
		$this->db->where('service_id',$service_id);
		$query = $this->db->get();
		//Insercion Si solo hay un tipo de item
		if($query->num_rows()<1 && count($item_kit_items)==1)
		{
			$success =$this->db->insert('item_kit_items',
						array(
						'service_id'=>$service_id,
						'item_id'=>key($item_kit_items),
						'quantity'=>current($item_kit_items)));
			
			
		}
		//Insercion si ya habia un item y se agrega uno mas
		if($query->num_rows()==1 && count($item_kit_items)==1)
		{
			
			$this->db->delete('item_kit_items', array('service_id' => $service_id));
			$success =$this->db->insert('item_kit_items',
						array(
						'service_id'=>$service_id,
						'item_id'=>key($item_kit_items),
						'quantity'=>current($item_kit_items)));
			
		}
		//Insercion si ya habia un item y se agrega mas de uno
		if(count($item_kit_items)>1)
		{
			$this->db->delete('item_kit_items', array('service_id' => $service_id));
			$success =$this->db->insert('item_kit_items',
						array(
						'service_id'=>$service_id,
						'item_id'=>key($item_kit_items),
						'quantity'=>current($item_kit_items)));
			foreach($item_kit_items as $itemq)
					{
						if(key($item_kit_items)!=null)
						{
						$success = $this->db->insert('item_kit_items',
						array(
						'service_id'=>$service_id,
						'item_id'=>key($item_kit_items),
						'quantity'=>current($item_kit_items)));
						}
					}
		}
		//Quita un item cuando solo hay dos
		if(count($item_kit_items)==1)
		{
			$this->db->delete('item_kit_items', array('service_id' => $service_id));
			$success =$this->db->insert('item_kit_items',
						array(
						'service_id'=>$service_id,
						'item_id'=>key($item_kit_items),
						'quantity'=>current($item_kit_items)));
			
		}
		//Borra todo si no hay items que guardar
		if(count($item_kit_items)<1)
		{
			$this->db->delete('item_kit_items', array('service_id' => $service_id));
		}
		$this->db->trans_complete();
		return $success;
		 
	}
		
	
	
	
	function delete($service_id)
	{
		return $this->db->delete('item_kit_items', array('service_id' => $service_id)); 
	}
}
?>
