<?php
require_once("report.php");
class Summary_suppliers extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array($this->lang->line('reports_supplier'),$this->lang->line('reports_quantity_purchased'), $this->lang->line('reports_total'),$this->lang->line('reports_almacenista'));
	}
	
	public function getData(array $inputs)
	{
		$this->db->select('CONCAT(first_name, " ",last_name) as supplier, sum(total) as total, sum(quantity_purchased) as quantity_purchased', false);
		$this->db->from('receivings_items_temp');
		$this->db->join('suppliers', 'suppliers.person_id = receivings_items_temp.supplier_id');
		$this->db->join('people', 'suppliers.person_id = people.person_id');
		$this->db->where('receiving_date BETWEEN "'.$inputs['start_date'].' 00-00-00" and "'.$inputs['end_date'].' 23-59-59"');
		
		$this->db->group_by('supplier_id');
		$this->db->order_by('last_name');
		
		return $this->db->get()->result_array();
		
	}
	
	public function getSummaryData(array $inputs)
	{
		$this->db->select('sum(total) as total');
		$this->db->from('receivings_items_temp');
		$this->db->join('suppliers', 'suppliers.person_id = receivings_items_temp.supplier_id');
		$this->db->join('people', 'suppliers.person_id = people.person_id');
		$this->db->where('receiving_date BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		

		return $this->db->get()->row_array();
	}
}
?>