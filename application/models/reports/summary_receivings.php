<?php
require_once("report.php");
class Summary_receivings extends Report
{
	function __construct()
	{
		parent::__construct();
	}

	public function getDataColumns()
	{
		return array(
		$this->lang->line('reports_almacenista'), 
		$this->lang->line('reports_total') 
		);
	}
	
	public function getData(array $inputs)
	{		
		$this->db->select('CONCAT(first_name, " ",last_name) as almacenista,receiving_date,payment_type,receiving_id,sum(total) as total', false);
		$this->db->from('receivings_items_temp');
		$this->db->join('employees', 'employees.person_id = receivings_items_temp.employee_id');
		$this->db->join('people', 'employees.person_id = people.person_id');
		$this->db->where('receiving_date BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		
		$this->db->group_by('employee_id');
		$this->db->order_by('last_name');

		return $this->db->get()->result_array();
	}
	
	public function getSummaryData(array $inputs)
	{
		$this->db->select('sum(total) as total');
		$this->db->from('receivings_items_temp');
		$this->db->join('employees', 'employees.person_id = receivings_items_temp.employee_id');
		$this->db->join('people', 'employees.person_id = people.person_id');
		$this->db->where('receiving_date BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		
		
		return $this->db->get()->row_array();
	}

}
?>