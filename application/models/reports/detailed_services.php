<?php
require_once("report.php");
class Detailed_services extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array('summary' => array(
										$this->lang->line('report_service_fecha'),
										$this->lang->line('report_service_nom_destinatario'), 
										$this->lang->line('report_service_nom_remitente'), 
										$this->lang->line('report_service_estado'), 
										),
					 'details' => array(
										$this->lang->line('report_service_motivo'), 
										$this->lang->line('report_service_accion'),
										$this->lang->line('report_service_reporte'),
										$this->lang->line('report_service_duracion')
										)
		);			
	}
	
	public function getData(array $inputs)
	{
		$this->db->select('service_id,fecha,nom_cliente,nom_asignado,estado');
		$this->db->from('services');
		$this->db->where('fecha BETWEEN "'. $inputs['start_date']. ' 00-00-00" and "'. $inputs['end_date'].' 23-59-59"');
		/*
		if ($inputs['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($inputs['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		*/
		//$this->db->group_by('sale_id');
		$this->db->order_by('fecha');

		$data = array();
		$data['summary'] = $this->db->get()->result_array();
		$data['details'] = array();
		
		foreach($data['summary'] as $key=>$value)
		{
			$this->db->select('motivo,accion,reporte,duracion');
			$this->db->from('services');
			$this->db->where('service_id = '.$value['service_id']);
			$data['details'][$key] = $this->db->get()->result_array();
		}
		
		return $data;
	}
	
	public function getSummaryData(array $inputs)
	{
		$this->db->select('count(service_id) as service_id');
		$this->db->from('services');
		$this->db->where('fecha BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		/*if ($inputs['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($inputs['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}*/
		
		return $this->db->get()->row_array();
	}
}
?>