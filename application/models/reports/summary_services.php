<?php
//require_once("report_b.php");
class Summary_services extends Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array($this->lang->line('reports_item'), $this->lang->line('reports_cantidad_prestada'), $this->lang->line('reports_cantidad_almacen'), $this->lang->line('reports_cantidad_total'),$this->lang->line('reports_item_cost_p'));
	}
	
	public function getData()
	{
		$this->db->select('item_kit_items.item_id,items.name,items.quantity,items.cost_price');
		$this->db->select_sum('item_kit_items.quantity', 'quantity_p');
		
		$this->db->from('item_kit_items');
		$this->db->join('services', 'item_kit_items.service_id = services.service_id','INNER');
		$this->db->join('items', 'item_kit_items.item_id = items.item_id','INNER');
		//$this->db->where('estado', 'Pendiente');
		
		
		
		
		$this->db->group_by('item_id');
		$this->db->order_by('item_id');
		return $this->db->get()->result_array();
		
	}
	
	public function getSummaryData()
	{
		$this->db->select('name');
		$this->db->from('services_temp');
		$this->db->join('items', 'sales_items_temp.item_id = items.item_id');
		$this->db->where('sale_date BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		if ($inputs['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($inputs['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		//return $this->db->get()->row_array();
	}
}
?>