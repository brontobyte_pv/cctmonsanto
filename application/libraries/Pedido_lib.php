<?php
class Pedido_lib
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();
	}

	function get_cart()
	{
		if(!$this->CI->session->userdata('cart'))
			$this->set_cart(array());

		return $this->CI->session->userdata('cart');
	}
	function set_cart($cart_data)
	{
		$this->CI->session->set_userdata('cart',$cart_data);
	}
	
	function get_items_totales()
	{
			$items_totales=0;
			if(!$this->CI->session->userdata('tots'))
			$this->CI->session->set_userdata('tots',$items_totales);

		return $this->CI->session->userdata('tots');
	}


	
	function add_item($item_id,$quantity=1,$discount=0,$price=null,$description=null,$serialnumber=null)
	{
		//make sure item exists
		if(!$this->CI->Item->exists($item_id))
		{
			//try to get item id given an item_number
			$item_id = $this->CI->Item->get_item_id($item_id);

			if(!$item_id)
				return false;
		}


		//Alain Serialization and Description

		//Get all items in the cart so far...
		$items = $this->get_cart();

        //We need to loop through all items in the cart.
        //If the item is already there, get it's key($updatekey).
        //We also need to get the next key that we are going to use in case we need to add the
        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.

        $maxkey=0;                       //Highest key so far
        $itemalreadyinsale=FALSE;        //We did not find the item yet.
		$insertkey=0;                    //Key to use for new entry.
		$updatekey=0;                    //Key to use to update(quantity)
		$subtotal=0;
		
		foreach ($items as $item)
		{
            //We primed the loop so maxkey is 0 the first time.
            //Also, we have stored the key in the element itself so we can compare.

			if($maxkey <= $item['line'])
			{
				$maxkey = $item['line'];
			}

			if($item['item_id']==$item_id)
			{
				$itemalreadyinsale=TRUE;
				$updatekey=$item['line'];
			}
			
		}

		$insertkey=$maxkey+1;
		$subtotal=$quantity*$this->CI->Item->get_info($item_id)->unit_price;
		//array/cart records are identified by $insertkey and item_id is just another field.
	
		$item = array(($insertkey)=>
		array(
			'item_id'=>$item_id,
			'line'=>$insertkey,
			'name'=>$this->CI->Item->get_info($item_id)->name,
			'quantity'=>$quantity,
            'discount'=>$discount,
			'price'=>$price!=null ? $price: $this->CI->Item->get_info($item_id)->unit_price,
			'subtotal'=>to_currency_no_money($subtotal)
			)
		);
		
		
		

		//Item already exists and is not serialized, add to quantity
		if($itemalreadyinsale)
		{
			$items[$updatekey]['quantity']+=$quantity;
			$items[$updatekey]['subtotal']+=$quantity*$this->CI->Item->get_info($item_id)->unit_price;
			

		}
		else
		{
			//add to existing array
			$items+=$item;
		}
		
		
		
		$items_totales=$this->get_items_totales()+1;
		
		$this->CI->session->set_userdata('tots',$items_totales);
		$this->set_cart($items);
		return true;

	}
		function get_subtotal()
	{
		$subtotal = 0;
		foreach($this->get_cart() as $item)
		{
		    $subtotal+=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100);
		}
		return to_currency_no_money($subtotal);
	}
	function get_total()
	{
		$total = 0;
		foreach($this->get_cart() as $item)
		{
            $total+=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100);
		}

		

		return to_currency_no_money($total);
	}
	function empty_cart()
	{
		$this->CI->session->unset_userdata('cart');
		$this->CI->session->unset_userdata('tots');
	}
	function item_in_cart($item_id)
	{
		 $itemalreadyinsale=FALSE; 
		 $items = $this->get_cart();
		 foreach ($items as $item)
		{
			if($item['item_id']==$item_id)
			{
				$itemalreadyinsale=TRUE;
			
			}
			
		}
		return $itemalreadyinsale;
		 
	}
	function delete_item($line)
	{
		$items_totales=$this->get_items_totales()-1;
		
		$this->CI->session->set_userdata('tots',$items_totales);
		$items=$this->get_cart();
		unset($items[$line]);
		$this->set_cart($items);
	}
	
	
}
?>
