<?php
class Lang_lib
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();
	}

	function get_idioma()
	{
		
		return $this->CI->session->userdata('idioma');
	}

	function set_idioma($idioma)
	{
		$this->CI->session->set_userdata('idioma',$idioma);
	}
	
	
}
?>
