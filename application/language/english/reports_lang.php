<?php
$lang['reports_reports'] = 'Reports';
$lang['reports_report'] = 'Report';
$lang['reports_welcome_message'] = 'Welcome to the reports panel. Please select a report to view.';
$lang['reports_sales_summary_report'] = 'Sales Summary Report';
$lang['reports_categories_summary_report'] = 'Categories Summary Report';
$lang['reports_customers_summary_report'] = 'Customers Summary Report';
$lang['reports_suppliers_summary_report'] = 'Suppliers Summary Report';
$lang['reports_items_summary_report'] = 'Items Summary Report';
$lang['reports_employees_summary_report'] = 'Employees Summary Report';
$lang['reports_taxes_summary_report'] = 'Taxes Summary Report';
$lang['reports_date'] = 'Date';
$lang['reports_name'] = 'Name';
$lang['reports_quantity_purchased'] = 'Quantity Purchased';
$lang['reports_sale_id'] = 'Sale ID';
$lang['reports_items_purchased'] = 'Items Purchased';
$lang['reports_sold_by'] = 'Sold By';
$lang['reports_sold_to'] = 'Sold To';
$lang['reports_category'] = 'Category';
$lang['reports_item'] = 'Item';
$lang['reports_items'] = 'Items';
$lang['reports_supplier'] = 'Supplier';
$lang['reports_subtotal'] = 'Subtotal';
$lang['reports_total'] = 'Total';
$lang['reports_tax'] = 'Tax';
$lang['reports_profit'] = 'Profit';
//$lang['reports_report_input'] = 'Report Input';
$lang['reports_type'] = 'Type';
$lang['reports_date_range'] = 'Date Range';
$lang['reports_today'] = 'Today';
$lang['reports_yesterday'] = 'Yesterday';
$lang['reports_last_7'] = 'Last 7 Days';
$lang['reports_this_month'] = 'This Month';
$lang['reports_last_month'] = 'Last Month';
$lang['reports_this_year'] = 'This Year';
$lang['reports_last_year'] = 'Last Year';
$lang['reports_all_time'] = 'All Time';
$lang['reports_detailed_sales_report'] = 'Detailed Sales Report';
$lang['reports_comments'] = 'Comments';
$lang['reports_discount'] = 'Discount';
$lang['reports_sales'] = 'Sales';
$lang['reports_categories'] = 'Categories';
$lang['reports_customers'] = 'Customers';
$lang['reports_suppliers'] = 'Suppliers';
$lang['reports_employees'] = 'Employees';
$lang['reports_taxes'] = 'Taxes';
$lang['reports_customer'] = 'Customer';
$lang['reports_employee'] = 'Employee';
$lang['reports_tax_percent'] = 'Tax Percent';
$lang['reports_serial_number'] = 'Serial #';
$lang['reports_description'] = 'Description';
$lang['reports_sales_amount'] = 'Sales amount';
$lang['reports_revenue'] = 'Revenue';
$lang['reports_discounts'] = 'Discounts';
$lang['reports_discounts_summary_report'] = 'Discounts Summary Report';
$lang['reports_discount_percent'] = 'Discount Percent';
$lang['reports_count'] = 'Count';
$lang['reports_summary_reports'] = 'Summary Reports';
$lang['reports_graphical_reports'] = 'Graphical Reports';
$lang['reports_detailed_reports'] = 'Detailed Reports';
$lang['reports_inventory_reports'] = 'Inventory Reports';
$lang['reports_low_inventory'] = 'Low Inventory';
$lang['reports_inventory_summary'] = ' Inventory Summary';
$lang['reports_item_number'] = 'Item Number';
$lang['reports_reorder_level'] = 'Reorder Level';
$lang['reports_low_inventory_report'] = 'Low Inventory Report';
$lang['reports_item_name'] = 'Item Name';
$lang['reports_inventory_summary_report'] = 'Inventory Summary Report';
$lang['reports_payment_type'] = 'Payment Type';
$lang['reports_payments_summary_report'] = 'Payments Summary Report';
$lang['reports_payments'] = 'Payments';
$lang['reports_receivings'] = 'Receivings';
$lang['reports_received_by'] = 'Received By';
$lang['reports_supplied_by'] = 'Supplied by';
$lang['reports_items_received'] = 'Items Received';
$lang['reports_detailed_receivings_report'] = 'Detailed Receivings Report';
$lang['reports_sale_type'] = 'Sale Type';
$lang['reports_all'] = 'All';
$lang['reports_returns'] = 'Returns';

$lang['reports_services'] = 'Services';
$lang['reports_services_summary_report']='Services Summary Report';


$lang['reports_detailed_services_report']='Detailed Report Services';
$lang['report_service_nom_destinatario']='Delivered to';
$lang['report_service_nom_remitente']='Delivered by';

$lang['report_service_fecha']='Date';
$lang['report_service_nom_cliente']='Customer Name';
$lang['report_service_nom_asignado']='Manager Name';
$lang['report_service_motivo']='Reason';
$lang['report_service_accion']='Action';
$lang['report_service_reporte']='Report';
$lang['report_service_estado']='Status';
$lang['report_service_duracion']='Duration';

$lang['reports_expenses_summary_report']='Expenses Summary Report';

$lang['reports_gastos']='Expenses';
$lang['reports_gastos_summary_gasto']='Expenses Summary Report';
$lang['reports_gastos_fehca']='Date';
$lang['reports_gastos_concepto']='Concept';
$lang['reports_gastos_motivo']='Reason';
$lang['reports_gastos_estado']='Status';
$lang['reports_gastos_cantidad']='Amount';
$lang['reports_gastos_tipo']='Type';

$lang['reports_cobros']='Charges';
$lang['reports_cobros_summary_report']=' Summary Report';

$lang['title_input_report_summary_sales']='Report Input of Sales';
$lang['title_input_report_summary_categories']='Report Input of Categories';
$lang['title_input_report_summary_customers']='Report Input of Customers';
$lang['title_input_report_summary_suppliers']='Report Input of Suppliers';
$lang['title_input_report_summary_items']='Report Input of Items';
$lang['title_input_report_summary_employees']='Report Input of Employees';
$lang['title_input_report_summary_taxes']='Report Input of Taxes';
$lang['title_input_report_summary_discounts']='Report Input of Discounts';
$lang['title_input_report_summary_payments']='Report Input of Payments';
$lang['title_input_report_summary_services']='Report Input of Services';
$lang['title_input_report_summary_cobros']='Report Input of Charges';
$lang['title_input_report_summary_gastos']='Report Input of Expenses';


?>
