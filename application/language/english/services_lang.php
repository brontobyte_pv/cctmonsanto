<?php
$lang['services_cliente']='Client';
$lang['services_motivo']='Reason';
$lang['services_date']='Date';
$lang['services_estado']='Estatus';
$lang['services_asignado']='Asigned';
$lang['services_accion']='To do';
$lang['services_duracion']='Estimated time';
$lang['services_reporte']='Report';
$lang['services_cambiar']='Change';
$lang['services_nuevo_servicio']='New Service';
$lang['services_hecho']='Done';
$lang['services_pendiente']='In procces';
$lang['services_cancelado']='Cancel';
$lang['services_confirm_delete']='Do you cancel this service?';

$lang['services_none_selected']='You Haven´t select any service';
$lang['services_cancel_success']='Service Canceled';
?>