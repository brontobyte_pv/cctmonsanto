<?php
$lang['module_home']='Home';

$lang['module_customers']='Customers';
$lang['module_customers_desc']='Add, Update, Delete, and Search customers';

$lang['module_suppliers']='Suppliers';
$lang['module_suppliers_desc']='Add, Update, Delete, and Search suppliers';

$lang['module_employees']='Employees';
$lang['module_employees_desc']='Add, Update, Delete, and Search employees';

$lang['module_sales']='Sales';
$lang['module_sales_desc']='Process sales and returns';

$lang['module_reports']='Reports';
$lang['module_reports_desc']='View and generate reports';

$lang['module_items']='Items';
$lang['module_items_desc']='Add, Update, Delete, and Search items';

$lang['module_config']='Store Config';
$lang['module_config_desc']='Change the store\'s configuration';

$lang['module_receivings']='Receivings';
$lang['module_receivings_desc']='Process Purchase orders';

$lang['module_giftcards']='Gift Cards';
$lang['module_giftcards_desc']='Add, Update, Delete and Search gift cards';

$lang['module_item_kits']='Item Kits';
$lang['module_item_kits_desc']='Add, Update, Delete and Search Item Kits';

$lang['module_services']='Services';
$lang['module_services_desc']='Add, Update, Delete and Search Services';

$lang['module_cobros']='Charges';
$lang['module_cobros_desc']='Add, Update, Delete and Search Charges';

$lang['module_gastos']='Expenses';
$lang['module_gastos_desc']='Add, Update, Delete and Search Expenses';

$lang['module_pedidos']='Order';
$lang['module_pedidos_desc']='Add, Update, Delete and Search Orders';

$lang['module_recursos']='Recursos';
$lang['module_recursos_desc']='Agregar, Actualizar, Borrar y Buscar Pedidos';

$lang['module_home']='Home';

?>
