<?php
$lang['module_home']='Inicio';

$lang['module_customers']='Empleados';
$lang['module_customers_desc']='Agregar, Actualizar, Borrar y Buscar Empleados';

$lang['module_suppliers']='Proveedores';
$lang['module_suppliers_desc']='Agregar, Actualizar, Borrar y Buscar proveedores';

$lang['module_employees']='Usuarios';
$lang['module_employees_desc']='Agregar, Actualizar, Borrar y Buscar Usuarios';

$lang['module_sales']='Salidas';
$lang['module_sales_desc']='Procesar Salidas y Entradas';

$lang['module_reports']='Reportes';
$lang['module_reports_desc']='Ver y generar reportes';

$lang['module_items']='Productos';
$lang['module_items_desc']='Agregar, Actualizar, Borrar y Buscar Materiales y ó Productos';

$lang['module_config']='Configuración';
$lang['module_config_desc']='Cambiar la configuración de la tienda';

$lang['module_receivings']='Entradas';
$lang['module_receivings_desc']='Procesar ordenes de Entradas';

$lang['module_giftcards']='Tarjetas';
$lang['module_giftcards_desc']='Agregar, Actualizar, Borrar y Buscar Tarjetas de Regalo';

$lang['module_item_kits']='Kits';
$lang['module_item_kits_desc']='Agregar, Actualizar, Borrar y Buscar Kits de Artículos';

$lang['module_services']='Prestamos de equipo';
$lang['module_services_desc']='Agregar, Actualizar, Borrar y Buscar Prestamos de Equipo';

$lang['module_cobros']='Cobros';
$lang['module_cobros_desc']='Agregar, Actualizar, Borrar y Buscar Cobros';

$lang['module_gastos']='Gastos';
$lang['module_gastos_desc']='Agregar, Actualizar, Borrar y Buscar Gastos';

$lang['module_pedidos']='Pedidos';
$lang['module_pedidos_desc']='Agregar, Actualizar, Borrar y Buscar Pedidos';

$lang['module_recursos']='Recursos';
$lang['module_recursos_desc']='Agregar, Actualizar, Borrar y Buscar Pedidos';

$lang['module_home']='Inicio';


?>
