<?php
$lang['services_new']='Nuevo Prestamo';
$lang['services_select_bef']='Seleccione un empleado antes de prestar equipo';
$lang['services_cliente']='Prestado a';
$lang['services_motivo']='Buscar Equipo';
$lang['services_date']='Fecha';
$lang['services_estado']='Estado';
$lang['services_asignado']='Prestado por';
$lang['services_accion']='Acción a realizar';
$lang['services_reporte']='Reporte';
$lang['services_duracion']='Duracion';
$lang['services_cambiar']='Cambiar';
$lang['services_nuevo_servicio']='Nuevo Prestamo de equipo';
$lang['services_hecho']='Hecho';
$lang['services_pendiente']='Pendiente';
$lang['services_cancelado']='Cancelado';
$lang['services_confirm_delete']='Esta seguro que desea cancelar este Prestamo de equipo';
$lang['services_none_selected']='No ha seleccionado ningun Prestamo de equipo';
$lang['services_cancel_success']='El Prestamo de equipo se ha cancelado de manera exitosa';
?>


