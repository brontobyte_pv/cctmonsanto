<?php
$lang['customers_new']='Nuevo Empleado';
$lang['customers_customer']='Empleado';
$lang['customers_update']='Actualizar Empleado';
$lang['customers_confirm_delete']='¿Estás seguro(a) de que quieres borrar a los Empleados seleccionados?';
$lang['customers_none_selected']='No has selccionado Empleados para ser borrados';
$lang['customers_error_adding_updating'] = 'Error agregando/actualizando Empleado';
$lang['customers_successful_adding']='Has agregado satisfactoriamente al Empleado';
$lang['customers_successful_updating']='Se ha actualizado correctamente al Empleado';
$lang['customers_successful_deleted']='Has borrado satisfactoriamente a';
$lang['customers_one_or_multiple']='Empleado(s)';
$lang['customers_cannot_be_deleted']='No se pudo borrar a los Empleados seleccionados. Uno o más de éstos tiene ventas.';
$lang['customers_basic_information']='Información de Empleados';
$lang['customers_account_number']='Cuenta #';
$lang['customers_taxable']='Gravable';
$lang['customers_company']='Código de empleado';
?>
