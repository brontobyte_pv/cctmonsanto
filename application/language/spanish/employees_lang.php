<?php
$lang['employees_madeby']='Recibió';
$lang['employees_employee']='Usuario';
$lang['employees_new']='Nuevo Usuario';
$lang['employees_update']='Actualizar Usuario';
$lang['employees_confirm_delete']='¿Estás seguro(a) quie quieres borrar a los Usuarios seleccionados?';
$lang['employees_none_selected']='No has seleccionado Usuarios para borrar';
$lang['employees_error_adding_updating'] = 'Error al agregar/actualizar Usuario';
$lang['employees_successful_adding']='Has agregado al Usuario satisfactoriamente';
$lang['employees_successful_updating']='Has actualizado al Usuario satisfactoriamente';
$lang['employees_successful_deleted']='Has borrado satisfactoriamente a';
$lang['employees_one_or_multiple']='Usuario(s)';
$lang['employees_cannot_be_deleted']='No se pudieron borrar Usuarios. Uno o más Usuarios tiene ventas procesadas o estás tratando de borrarte a tí mismo(a).';
$lang['employees_username']='Usuario';
$lang['employees_password']='Contraseña';
$lang['employees_repeat_password']='Contraseña Otra Vez';
$lang['employees_username_required']='Usuario es requerido';
$lang['employees_username_minlength']='El Usuario debe tener, por lo menos, 5 caracteres';
$lang['employees_password_required']='La Contraseña es requerida';
$lang['employees_password_minlength']='La contraseña debe tener, por lo menos, 8 caracteres';
$lang['employees_password_must_match']='Las Contraseñas no coinciden';
$lang['employees_basic_information']='Información Básica de Usuarios';
$lang['employees_login_info']='Información de Ingreso del Usuario';
$lang['employees_permission_info']='Permisos y Acceso del Usuario';
$lang['employees_permission_desc']='Activa las cajas debajo para permitir el acceso a los módulos';
$lang['employees_error_updating_demo_admin'] = 'No puedes cambiar el usuario admin del demo';
$lang['employees_error_deleting_demo_admin'] = 'No puedes borrar el usuario admin del demo';
?>
