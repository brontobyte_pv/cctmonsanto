#
# TABLE STRUCTURE FOR: ospos_app_config
#

DROP TABLE IF EXISTS ospos_app_config;

CREATE TABLE `ospos_app_config` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO ospos_app_config (`key`, `value`) VALUES ('address', 'Venustiano Carranza#381');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('company', 'Brontobyte');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('currency_symbol', '');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('default_tax_1_name', 'Sales Tax');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('default_tax_1_rate', '');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('default_tax_2_name', 'Sales Tax 2');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('default_tax_2_rate', '');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('default_tax_rate', '8');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('email', 'adrian@brontobytemx.com');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('fax', '');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('language', 'spanish');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('phone', '3221584600');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('print_after_sale', '0');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('return_policy', 'Test');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('timezone', 'America/New_York');
INSERT INTO ospos_app_config (`key`, `value`) VALUES ('website', '');


#
# TABLE STRUCTURE FOR: ospos_cobros
#

DROP TABLE IF EXISTS ospos_cobros;

CREATE TABLE `ospos_cobros` (
  `cobro_id` int(255) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `nom_cliente` varchar(20) NOT NULL,
  `concepto` varchar(255) NOT NULL,
  `estado` varchar(15) NOT NULL DEFAULT 'Pendiente',
  `cantidad` double(10,2) NOT NULL DEFAULT '0.00',
  `tipo` varchar(15) NOT NULL DEFAULT 'Efectivo',
  `recurso_id` int(10) NOT NULL,
  `sale_id` int(10) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`cobro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ospos_customers
#

DROP TABLE IF EXISTS ospos_customers;

CREATE TABLE `ospos_customers` (
  `person_id` int(10) NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `taxable` int(1) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `company` varchar(255) DEFAULT NULL,
  UNIQUE KEY `account_number` (`account_number`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `ospos_customers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (3, NULL, 1, 1, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (5, NULL, 1, 1, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (12, NULL, 1, 1, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (13, NULL, 1, 1, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (14, NULL, 1, 1, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (15, NULL, 1, 1, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (16, NULL, 1, 1, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (17, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (18, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (19, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (20, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (21, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (22, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (23, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (24, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (25, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (26, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (27, NULL, 1, 1, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (28, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (29, NULL, 1, 1, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (30, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (31, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (32, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (33, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (34, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (35, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (36, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (37, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (38, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (39, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (40, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (41, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (42, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (43, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (44, NULL, 1, 0, '');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (46, NULL, 1, 0, '232132');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (47, NULL, 1, 0, '65654');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (48, NULL, 1, 0, '342342');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (49, NULL, 1, 0, '4123423');
INSERT INTO ospos_customers (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES (50, NULL, 1, 0, '423423');


#
# TABLE STRUCTURE FOR: ospos_employees
#

DROP TABLE IF EXISTS ospos_employees;

CREATE TABLE `ospos_employees` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `username` (`username`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `ospos_employees_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO ospos_employees (`username`, `password`, `person_id`, `deleted`) VALUES ('NaN', '21232f297a57a5a743894a0e4a801fc3', 1, 0);


#
# TABLE STRUCTURE FOR: ospos_gastos
#

DROP TABLE IF EXISTS ospos_gastos;

CREATE TABLE `ospos_gastos` (
  `gasto_id` int(255) NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `concepto` varchar(255) NOT NULL,
  `recurso_id` int(10) NOT NULL,
  `estado` varchar(15) NOT NULL DEFAULT 'Pendiente',
  `cantidad` double(10,2) NOT NULL DEFAULT '0.00',
  `tipo` varchar(15) NOT NULL DEFAULT 'Efectivo',
  PRIMARY KEY (`gasto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ospos_giftcards
#

DROP TABLE IF EXISTS ospos_giftcards;

CREATE TABLE `ospos_giftcards` (
  `giftcard_id` int(11) NOT NULL AUTO_INCREMENT,
  `giftcard_number` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `value` double(15,2) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`giftcard_id`),
  UNIQUE KEY `giftcard_number` (`giftcard_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# TABLE STRUCTURE FOR: ospos_inventory
#

DROP TABLE IF EXISTS ospos_inventory;

CREATE TABLE `ospos_inventory` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_items` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text NOT NULL,
  `trans_inventory` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`trans_id`),
  KEY `ospos_inventory_ibfk_1` (`trans_items`),
  KEY `ospos_inventory_ibfk_2` (`trans_user`),
  CONSTRAINT `ospos_inventory_ibfk_1` FOREIGN KEY (`trans_items`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_inventory_ibfk_2` FOREIGN KEY (`trans_user`) REFERENCES `ospos_employees` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO ospos_inventory (`trans_id`, `trans_items`, `trans_user`, `trans_date`, `trans_comment`, `trans_inventory`) VALUES (4, 4, 1, '2016-05-02 15:33:16', 'Edición Manual de Cantidad', 2);
INSERT INTO ospos_inventory (`trans_id`, `trans_items`, `trans_user`, `trans_date`, `trans_comment`, `trans_inventory`) VALUES (5, 5, 1, '2016-05-03 11:11:56', 'Edición Manual de Cantidad', 1);
INSERT INTO ospos_inventory (`trans_id`, `trans_items`, `trans_user`, `trans_date`, `trans_comment`, `trans_inventory`) VALUES (6, 6, 1, '2016-05-03 13:49:31', 'Edición Manual de Cantidad', 4);
INSERT INTO ospos_inventory (`trans_id`, `trans_items`, `trans_user`, `trans_date`, `trans_comment`, `trans_inventory`) VALUES (7, 6, 1, '2016-05-04 13:26:04', 'POS 1', -24);
INSERT INTO ospos_inventory (`trans_id`, `trans_items`, `trans_user`, `trans_date`, `trans_comment`, `trans_inventory`) VALUES (8, 7, 1, '2016-06-01 13:53:45', 'Edición Manual de Cantidad', 2);
INSERT INTO ospos_inventory (`trans_id`, `trans_items`, `trans_user`, `trans_date`, `trans_comment`, `trans_inventory`) VALUES (9, 7, 1, '2016-06-20 19:26:38', 'RECV 1', 1);


#
# TABLE STRUCTURE FOR: ospos_item_kit_items
#

DROP TABLE IF EXISTS ospos_item_kit_items;

CREATE TABLE `ospos_item_kit_items` (
  `service_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` double(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`service_id`,`item_id`,`quantity`),
  KEY `ospos_item_kit_items_ibfk_2` (`item_id`),
  CONSTRAINT `ospos_item_kit_items_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `ospos_services` (`service_id`) ON DELETE CASCADE,
  CONSTRAINT `ospos_item_kit_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (15, 4, '5.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (21, 4, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (23, 4, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (31, 4, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (35, 4, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (40, 4, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (42, 4, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (1, 6, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (13, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (14, 7, '15.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (16, 7, '5.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (17, 7, '2.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (18, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (19, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (20, 7, '2.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (22, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (24, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (25, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (26, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (27, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (28, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (29, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (30, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (32, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (33, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (34, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (36, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (37, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (38, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (39, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (41, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (43, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (44, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (45, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (46, 7, '1.00');
INSERT INTO ospos_item_kit_items (`service_id`, `item_id`, `quantity`) VALUES (47, 7, '1.00');


#
# TABLE STRUCTURE FOR: ospos_item_kits
#

DROP TABLE IF EXISTS ospos_item_kits;

CREATE TABLE `ospos_item_kits` (
  `item_kit_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `kit_number` varchar(255) DEFAULT NULL,
  `category` varchar(255) NOT NULL,
  `kit_price` double(10,2) NOT NULL DEFAULT '0.00',
  `publicar` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_kit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ospos_items
#

DROP TABLE IF EXISTS ospos_items;

CREATE TABLE `ospos_items` (
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `item_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `cost_price` double(15,2) NOT NULL DEFAULT '0.00',
  `unit_price` double(15,2) NOT NULL DEFAULT '0.00',
  `other_price` double(15,2) NOT NULL DEFAULT '0.00',
  `quantity` double(15,2) NOT NULL DEFAULT '0.00',
  `reorder_level` double(15,2) NOT NULL DEFAULT '0.00',
  `location` varchar(255) NOT NULL DEFAULT 'Principal',
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `allow_alt_description` tinyint(1) NOT NULL DEFAULT '0',
  `is_serialized` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `publicar` tinyint(1) NOT NULL DEFAULT '0',
  `promocionar` tinyint(4) NOT NULL DEFAULT '0',
  `brand` varchar(255) DEFAULT NULL,
  `modelo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `item_number` (`item_number`),
  KEY `ospos_items_ibfk_1` (`supplier_id`),
  CONSTRAINT `ospos_items_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `ospos_suppliers` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO ospos_items (`name`, `category`, `supplier_id`, `item_number`, `description`, `cost_price`, `unit_price`, `other_price`, `quantity`, `reorder_level`, `location`, `item_id`, `allow_alt_description`, `is_serialized`, `deleted`, `publicar`, `promocionar`, `brand`, `modelo`) VALUES ('arena', 'RIO', 4, '124212', 'lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj', '220.00', '1.00', '0.00', '2.00', '4.00', 'X', 4, 0, 0, 0, 0, 0, 'rio', 'metro');
INSERT INTO ospos_items (`name`, `category`, `supplier_id`, `item_number`, `description`, `cost_price`, `unit_price`, `other_price`, `quantity`, `reorder_level`, `location`, `item_id`, `allow_alt_description`, `is_serialized`, `deleted`, `publicar`, `promocionar`, `brand`, `modelo`) VALUES ('arena mexico-relkmnkjjkjoljfjdpoifjpojkdfpodfdpf', '22', 4, '2222', 'dasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasas', '23212.00', '1.00', '0.00', '1.00', '2.00', '13sq2w', 5, 0, 0, 1, 0, 0, '222', '222');
INSERT INTO ospos_items (`name`, `category`, `supplier_id`, `item_number`, `description`, `cost_price`, `unit_price`, `other_price`, `quantity`, `reorder_level`, `location`, `item_id`, `allow_alt_description`, `is_serialized`, `deleted`, `publicar`, `promocionar`, `brand`, `modelo`) VALUES ('gamesa donas de chocolate', 'califpornia', 6, '232133221122', 'aqui una breve descripcion de lo que es el producto prueba test test aqui una breve descripcion del proidiucto', '121211121.00', '1.00', '0.00', '-20.00', '4.00', 'lomas de las glorias', 6, 0, 0, 1, 0, 0, 'delisiosas', '1');
INSERT INTO ospos_items (`name`, `category`, `supplier_id`, `item_number`, `description`, `cost_price`, `unit_price`, `other_price`, `quantity`, `reorder_level`, `location`, `item_id`, `allow_alt_description`, `is_serialized`, `deleted`, `publicar`, `promocionar`, `brand`, `modelo`) VALUES ('Pantene', 'shampoo', NULL, '32649873640983265', 'shampo oijdpojsñoifhñhggñohkuhvbknbsfdhvbvsñodvb.skufdvbludfbvisudb', '90.00', '1.00', '0.00', '3.00', '2.00', 'almacen', 7, 0, 0, 0, 0, 0, 'belleza', 'dosis');


#
# TABLE STRUCTURE FOR: ospos_items_taxes
#

DROP TABLE IF EXISTS ospos_items_taxes;

CREATE TABLE `ospos_items_taxes` (
  `item_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `percent` double(15,2) NOT NULL,
  PRIMARY KEY (`item_id`,`name`,`percent`),
  CONSTRAINT `ospos_items_taxes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ospos_locations
#

DROP TABLE IF EXISTS ospos_locations;

CREATE TABLE `ospos_locations` (
  `nombre_location` varchar(255) NOT NULL,
  `item_id` int(20) NOT NULL,
  `cantidad` double(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ospos_modules
#

DROP TABLE IF EXISTS ospos_modules;

CREATE TABLE `ospos_modules` (
  `name_lang_key` varchar(255) NOT NULL,
  `desc_lang_key` varchar(255) NOT NULL,
  `sort` int(10) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE KEY `desc_lang_key` (`desc_lang_key`),
  UNIQUE KEY `name_lang_key` (`name_lang_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO ospos_modules (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES ('module_config', 'module_config_desc', 90, 'config');
INSERT INTO ospos_modules (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES ('module_customers', 'module_customers_desc', 50, 'customers');
INSERT INTO ospos_modules (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES ('module_employees', 'module_employees_desc', 80, 'employees');
INSERT INTO ospos_modules (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES ('module_items', 'module_items_desc', 30, 'items');
INSERT INTO ospos_modules (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES ('module_receivings', 'module_receivings_desc', 20, 'receivings');
INSERT INTO ospos_modules (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES ('module_reports', 'module_reports_desc', 70, 'reports');
INSERT INTO ospos_modules (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES ('module_sales', 'module_sales_desc', 40, 'sales');
INSERT INTO ospos_modules (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES ('module_services', 'module_services_desc', 60, 'services');
INSERT INTO ospos_modules (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES ('module_suppliers', 'module_suppliers_desc', 10, 'suppliers');


#
# TABLE STRUCTURE FOR: ospos_pedidos
#

DROP TABLE IF EXISTS ospos_pedidos;

CREATE TABLE `ospos_pedidos` (
  `pedido_id` int(255) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  PRIMARY KEY (`pedido_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ospos_people
#

DROP TABLE IF EXISTS ospos_people;

CREATE TABLE `ospos_people` (
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `person_id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('John', 'Doe', '555-555-5555', 'admin@pappastech.com', 'Address 1', '', '', '', '', '', '', 1);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('susana', 'j', '', '', '', '', '', '', '', '', '', 3);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('joaquin', 'lopez', '34532223232', '', '', '', '', '', '', '', '', 4);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('adrian', 'Ramirez', '35423332', '', '', '', '', '', '', '', '', 5);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('susana', 'jacinto', '', '', '', '', '', '', '', '', '', 6);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('antonia', 'lopez', '2123122222222', '', '', '', '', '', '', '', '', 7);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('nombre muy largo para hacer el test', 'apellido muy largo para hacer el test', '3445675455', '', '', '', '', '', '', '', '', 8);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('asaaaa', 'aaaaaa', '', '', '', '', '', '', '', '', '', 9);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('ddsd', 'dd', '', '', '', '', '', '', '', '', '', 10);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('w', 'w', '', '', '', '', '', '', '', '', '', 11);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('carla', 'morrison', '3425432212', '', '', '', '', '', '', '', '', 12);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('gustavo', 'cerati', '', '', '', '', '', '', '', '', '', 13);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('luis', 'miguel', '', '', '', '', '', '', '', '', '', 14);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('sed', 'sed', '', '', '', '', '', '', '', '', '', 15);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('ea', 'as', '', '', '', '', '', '', '', '', '', 16);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('carla', 'morrison', '', '', '', '', '', '', '', '', '', 17);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('JOSE', 'LOPEZ', '', '', '', '', '', '', '', '', '', 18);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('JOSE', 'LOPEZ', '', '', '', '', '', '', '', '', '', 19);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('RAUL', 'GOMEZ', '', '', '', '', '', '', '', '', '', 20);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('TANIA', 'FERNANDA', '', '', '', '', '', '', '', '', '', 21);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('e', 'w', '', '', '', '', '', '', '', '', '', 22);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('a', 'e', '', '', '', '', '', '', '', '', '', 23);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('sdf', 'saa', '', '', '', '', '', '', '', '', '', 24);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('sdf', 'saa', '', '', '', '', '', '', '', '', '', 25);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('w', 'www', '', '', '', '', '', '', '', '', '', 26);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('wwwwwwwww', 'ww', '', '', '', '', '', '', '', '', '', 27);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('dsdasdc', 'sdasd', '', '', '', '', '', '', '', '', '', 28);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('dasdz<asd', '|adsadsd', '', '', '', '', '', '', '', '', '', 29);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('dasdasdasscas', 'asdsdz', '', '', '', '', '', '', '', '', '', 30);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('sdasdad', 'sdasddasdas', '', '', '', '', '', '', '', '', '', 31);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('dsdassad', 'sdasdasdasxasdd', '', '', '', '', '', '', '', '', '', 32);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('casxazxsxc', 'scsd scas', '', '', '', '', '', '', '', '', '', 33);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('fscc', 'fcassdcd', '', '', '', '', '', '', '', '', '', 34);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('fscs', 'fsdsfrsfwrezdwfc', '', '', '', '', '', '', '', '', '', 35);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('fcwwfc', 'cdffwfwe', '', '', '', '', '', '', '', '', '', 36);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('ddsds', 'sdsadswdsd', '', '', '', '', '', '', '', '', '', 37);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('ol', 'ol', '', '', '', '', '', '', '', '', '', 38);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('zzz', 'zzzzz', '', '', '', '', '', '', '', '', '', 39);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('zasads', 'xxaxaxs', '', '', '', '', '', '', '', '', '', 40);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('yyy', 'vcxcvxcvxc', '', '', '', '', '', '', '', '', '', 41);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('asss', 'ssssss', '', '', '', '', '', '', '', '', '', 42);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('Fsdsdfs', 'sdfsdfsdf', '', '', '', '', '', '', '', '', '', 43);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('Alondra', 'Huerta', '', '', '', '', '', '', '', '', '0', 44);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('Susana', 'Jacinto', '', '', '', '', '', '', '', '', '', 45);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('pedro', 'pano', '213123', '', '', '', '', '', '', '', '0', 46);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('Sergio', 'Ramos', '546546', '', '', '', '', '', '', '', '0', 47);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('Selene', 'Gomez', '23323', '', '', '', '', '', '', '', '0', 48);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('Hel', 'Some', '32312323', '', '', '', '', '', '', '', '0', 49);
INSERT INTO ospos_people (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES ('Gerald', 'Marqueaz', '657657', '', '', '', '', '', '', '', '0', 50);


#
# TABLE STRUCTURE FOR: ospos_permissions
#

DROP TABLE IF EXISTS ospos_permissions;

CREATE TABLE `ospos_permissions` (
  `module_id` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  PRIMARY KEY (`module_id`,`person_id`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `ospos_permissions_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_employees` (`person_id`),
  CONSTRAINT `ospos_permissions_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `ospos_modules` (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO ospos_permissions (`module_id`, `person_id`) VALUES ('config', 1);
INSERT INTO ospos_permissions (`module_id`, `person_id`) VALUES ('customers', 1);
INSERT INTO ospos_permissions (`module_id`, `person_id`) VALUES ('employees', 1);
INSERT INTO ospos_permissions (`module_id`, `person_id`) VALUES ('items', 1);
INSERT INTO ospos_permissions (`module_id`, `person_id`) VALUES ('receivings', 1);
INSERT INTO ospos_permissions (`module_id`, `person_id`) VALUES ('reports', 1);
INSERT INTO ospos_permissions (`module_id`, `person_id`) VALUES ('sales', 1);
INSERT INTO ospos_permissions (`module_id`, `person_id`) VALUES ('services', 1);
INSERT INTO ospos_permissions (`module_id`, `person_id`) VALUES ('suppliers', 1);


#
# TABLE STRUCTURE FOR: ospos_receivings
#

DROP TABLE IF EXISTS ospos_receivings;

CREATE TABLE `ospos_receivings` (
  `receiving_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `receiving_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(20) DEFAULT NULL,
  `total` double(15,2) NOT NULL,
  `modo` varchar(20) NOT NULL DEFAULT 'Entrada',
  PRIMARY KEY (`receiving_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `ospos_receivings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`),
  CONSTRAINT `ospos_receivings_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `ospos_suppliers` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO ospos_receivings (`receiving_time`, `supplier_id`, `employee_id`, `comment`, `receiving_id`, `payment_type`, `total`, `modo`) VALUES ('2016-06-20 18:26:38', 45, 1, '', 1, '', '90.00', 'Entrada');


#
# TABLE STRUCTURE FOR: ospos_receivings_items
#

DROP TABLE IF EXISTS ospos_receivings_items;

CREATE TABLE `ospos_receivings_items` (
  `receiving_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` int(10) NOT NULL DEFAULT '0',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` double(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`receiving_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `ospos_receivings_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_receivings_items_ibfk_2` FOREIGN KEY (`receiving_id`) REFERENCES `ospos_receivings` (`receiving_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO ospos_receivings_items (`receiving_id`, `item_id`, `description`, `serialnumber`, `line`, `quantity_purchased`, `item_cost_price`, `item_unit_price`, `discount_percent`) VALUES (1, 7, 'shampo oijdpojsñoifhñhggñohkuh', '', 1, 1, '90.00', '90.00', 0);


#
# TABLE STRUCTURE FOR: ospos_recursos
#

DROP TABLE IF EXISTS ospos_recursos;

CREATE TABLE `ospos_recursos` (
  `recurso_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `recurso` double(15,2) NOT NULL,
  PRIMARY KEY (`recurso_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ospos_sales
#

DROP TABLE IF EXISTS ospos_sales;

CREATE TABLE `ospos_sales` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`sale_id`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `ospos_sales_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`),
  CONSTRAINT `ospos_sales_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `ospos_customers` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO ospos_sales (`sale_time`, `customer_id`, `employee_id`, `comment`, `sale_id`, `payment_type`) VALUES ('2016-05-04 13:26:04', 5, 1, '0', 1, '');


#
# TABLE STRUCTURE FOR: ospos_sales_item_kits
#

DROP TABLE IF EXISTS ospos_sales_item_kits;

CREATE TABLE `ospos_sales_item_kits` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_kit_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` double(15,2) NOT NULL DEFAULT '0.00',
  `kit_price` double(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`),
  KEY `item_kit_id` (`item_kit_id`),
  CONSTRAINT `ospos_sales_item_kits_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`),
  CONSTRAINT `ospos_sales_item__kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `ospos_item_kits` (`item_kit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ospos_sales_items
#

DROP TABLE IF EXISTS ospos_sales_items;

CREATE TABLE `ospos_sales_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` double(15,2) NOT NULL DEFAULT '0.00',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` double(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `ospos_sales_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_sales_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO ospos_sales_items (`sale_id`, `item_id`, `description`, `serialnumber`, `line`, `quantity_purchased`, `item_cost_price`, `item_unit_price`, `discount_percent`) VALUES (1, 6, 'aqui una breve descripcion de ', '', 1, '24.00', '121211121.00', '1.00', 0);


#
# TABLE STRUCTURE FOR: ospos_sales_items_taxes
#

DROP TABLE IF EXISTS ospos_sales_items_taxes;

CREATE TABLE `ospos_sales_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` double(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `ospos_sales_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales_items` (`sale_id`),
  CONSTRAINT `ospos_sales_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ospos_sales_payments
#

DROP TABLE IF EXISTS ospos_sales_payments;

CREATE TABLE `ospos_sales_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  `total` double(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`),
  CONSTRAINT `ospos_sales_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ospos_sales_suspended
#

DROP TABLE IF EXISTS ospos_sales_suspended;

CREATE TABLE `ospos_sales_suspended` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`sale_id`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `ospos_sales_suspended_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`),
  CONSTRAINT `ospos_sales_suspended_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `ospos_customers` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ospos_sales_suspended_items
#

DROP TABLE IF EXISTS ospos_sales_suspended_items;

CREATE TABLE `ospos_sales_suspended_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` double(15,2) NOT NULL DEFAULT '0.00',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` double(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `ospos_sales_suspended_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_sales_suspended_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales_suspended` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ospos_sales_suspended_items_taxes
#

DROP TABLE IF EXISTS ospos_sales_suspended_items_taxes;

CREATE TABLE `ospos_sales_suspended_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` double(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `ospos_sales_suspended_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales_suspended_items` (`sale_id`),
  CONSTRAINT `ospos_sales_suspended_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ospos_sales_suspended_payments
#

DROP TABLE IF EXISTS ospos_sales_suspended_payments;

CREATE TABLE `ospos_sales_suspended_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`),
  CONSTRAINT `ospos_sales_suspended_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales_suspended` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ospos_services
#

DROP TABLE IF EXISTS ospos_services;

CREATE TABLE `ospos_services` (
  `service_id` int(255) NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nom_cliente` varchar(50) NOT NULL,
  `nom_asignado` varchar(50) NOT NULL,
  `motivo` varchar(255) NOT NULL,
  `accion` text NOT NULL,
  `reporte` text NOT NULL,
  `estado` varchar(100) NOT NULL DEFAULT 'Pendiente',
  `duracion` double(10,1) NOT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (1, '2016-05-03 11:07:24', 'susana j', 'John Doe', '', '', '', 'Entregado', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (2, '2016-05-03 11:07:26', 'susana j', 'John Doe', '', '', '', 'Entregado', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (4, '2016-05-03 11:07:30', 'susana j', 'John Doe', '', '', '', 'Entregado', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (6, '2016-05-03 11:07:33', 'susana j', 'John Doe', '', '', '', 'Entregado', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (8, '2016-05-04 14:16:52', 'JOSE LOPEZ', 'John Doe', '', '', '', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (9, '2016-06-20 19:12:51', 'JOSE LOPEZ', 'John Doe', '', '0', '0', 'Entregado', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (10, '2016-06-20 19:12:40', 'JOSE LOPEZ', 'John Doe', '', '0', '0', 'Entregado', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (11, '2016-06-20 19:12:27', 'JOSE LOPEZ', 'John Doe', '', '0', '0', 'Entregado', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (12, '2016-06-20 19:12:12', 'JOSE LOPEZ', 'John Doe', '', '0', '0', 'Entregado', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (13, '2016-06-20 19:11:57', 'Alondra Huerta', 'John Doe', '', '0', '0', 'Entregado', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (14, '2016-06-20 19:14:00', 'TANIA FERNANDA', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (15, '2016-06-20 19:40:36', 'TANIA FERNANDA', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (16, '2016-06-20 19:43:18', 'RAUL GOMEZ', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (17, '2016-06-20 19:43:54', 'RAUL GOMEZ', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (18, '2016-06-29 21:04:55', 'asss ssssss', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (19, '2016-06-29 21:22:31', 'dsdasdc sdasd', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (20, '2016-06-29 21:27:11', 'TANIA FERNANDA', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (21, '2016-06-29 21:33:10', 'dsdassad sdasdasdasxasdd', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (22, '2016-06-29 21:36:32', 'Alondra Huerta', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (23, '2016-06-29 21:37:14', 'casxazxsxc scsd scas', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (24, '2016-06-29 21:40:57', 'pedro pano', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (25, '2016-06-29 21:43:04', 'RAUL GOMEZ', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (26, '2016-06-29 21:47:25', 'Fsdsdfs sdfsdfsdf', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (27, '2016-06-29 21:48:08', 'ddsds sdsadswdsd', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (28, '2016-06-29 21:51:07', 'asss ssssss', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (29, '2016-06-29 21:57:01', 'zzz zzzzz', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (30, '2016-06-29 21:59:23', 'zzz zzzzz', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (31, '2016-06-29 22:00:21', 'ol ol', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (32, '2016-06-29 22:03:18', 'zzz zzzzz', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (33, '2016-06-29 22:07:53', 'ol ol', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (34, '2016-06-29 22:10:39', 'yyy vcxcvxcvxc', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (35, '2016-06-29 22:14:40', 'zasads xxaxaxs', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (36, '2016-06-29 22:17:50', 'Selene Gomez', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (37, '2016-06-29 22:20:13', 'pedro pano', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (38, '2016-06-29 22:24:32', 'Alondra Huerta', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (39, '2016-06-29 22:25:23', 'Gerald Marqueaz', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (40, '2016-06-29 22:26:36', 'yyy vcxcvxcvxc', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (41, '2016-06-29 22:28:42', 'JOSE LOPEZ', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (42, '2016-06-29 22:30:16', 'zzz zzzzz', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (43, '2016-06-29 22:30:50', 'Sergio Ramos', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (44, '2016-06-29 22:32:40', 'zasads xxaxaxs', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (45, '2016-06-29 22:33:49', 'JOSE LOPEZ', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (46, '2016-06-29 22:43:17', 'carla morrison', 'John Doe', '', '0', '0', 'Pendiente', '0.0');
INSERT INTO ospos_services (`service_id`, `fecha`, `nom_cliente`, `nom_asignado`, `motivo`, `accion`, `reporte`, `estado`, `duracion`) VALUES (47, '2016-07-08 11:44:18', 'Selene Gomez', 'John Doe', '', '0', '0', 'Pendiente', '0.0');


#
# TABLE STRUCTURE FOR: ospos_sessions
#

DROP TABLE IF EXISTS ospos_sessions;

CREATE TABLE `ospos_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('077035a34d206bb4105522a9666fd061', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464796155, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('0890a2c18581c503a02058e941cff4ca', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1467996717, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('229487d0a25dad7721c096e14ece42c8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1462457816, 'a:2:{s:9:\"person_id\";s:1:\"1\";s:18:\"customer_servicios\";s:2:\"-1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('2b6d3391d93b98dcf72e28443ac0974c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1465401721, 'a:10:{s:9:\"person_id\";s:1:\"1\";s:18:\"customer_servicios\";s:2:\"-1\";s:4:\"cart\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}s:9:\"sale_mode\";s:4:\"sale\";s:8:\"customer\";s:2:\"-1\";s:8:\"payments\";a:0:{}s:9:\"data_save\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}s:8:\"cartRecv\";a:1:{i:1;a:10:{s:7:\"item_id\";s:1:\"7\";s:4:\"line\";i:1;s:4:\"name\";s:7:\"Pantene\";s:11:\"description\";s:71:\"shampo oijdpojsñoifhñhggñohkuhvbknbsfdhvbvsñodvb.skufdvbludfbvisudb\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:5:\"90.00\";}}s:9:\"recv_mode\";s:7:\"receive\";s:8:\"supplier\";s:2:\"-1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('2e3e63a0c7a878e6a3954123c8a5c2aa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1462300146, 'a:4:{s:9:\"person_id\";s:1:\"1\";s:8:\"cartRecv\";a:0:{}s:9:\"recv_mode\";s:7:\"receive\";s:8:\"supplier\";s:1:\"6\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('3180bbdabbcc7e5ecd806a9cfef9fb82', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464966472, 'a:10:{s:9:\"person_id\";s:1:\"1\";s:8:\"cartRecv\";a:1:{i:1;a:10:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:6:\"220.00\";}}s:9:\"recv_mode\";s:7:\"receive\";s:8:\"supplier\";s:2:\"-1\";s:4:\"cart\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:2;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}s:9:\"sale_mode\";s:4:\"sale\";s:8:\"customer\";s:2:\"-1\";s:8:\"payments\";a:0:{}s:18:\"customer_servicios\";s:2:\"-1\";s:9:\"data_save\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:2;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('326022f84a841103aa0ec5b0c14dac08', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464966425, 'a:1:{s:9:\"person_id\";s:1:\"1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('32836bb00d189457c56569de6fb6240a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464796152, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('3465caa25bb17fd2a4fb0bd5a907eea5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464202361, 'a:6:{s:9:\"person_id\";s:1:\"1\";s:18:\"customer_servicios\";s:2:\"-1\";s:4:\"cart\";a:0:{}s:9:\"sale_mode\";s:4:\"sale\";s:8:\"customer\";s:2:\"-1\";s:8:\"payments\";a:0:{}}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('34f9e004d4137fdf086215f1bffca557', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1467996723, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('3a89ce1ce7d41d32fc46608e77a998d1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464796157, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('43381f30e87ea1f0bd76cfedebaf8e58', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1462298255, 'a:4:{s:9:\"person_id\";s:1:\"1\";s:8:\"cartRecv\";a:0:{}s:9:\"recv_mode\";s:7:\"receive\";s:8:\"supplier\";s:2:\"11\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('44ab6ecab229dfa1ba8037604a87c42f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464977254, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('513db1d0769c1fe5aba33da335b5e187', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1467996715, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('56fac7d234c18941b38afe8748b839e5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1467996721, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('57a7801403db70d3c8ce965fca2b7939', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464102601, 'a:7:{s:9:\"person_id\";s:1:\"1\";s:4:\"kits\";a:0:{}s:9:\"data_save\";a:0:{}s:9:\"sale_mode\";s:4:\"sale\";s:4:\"cart\";a:0:{}s:8:\"customer\";s:2:\"43\";s:8:\"payments\";a:0:{}}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('67d562b5851fcd4b0c8472118a108a81', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1462299019, 'a:4:{s:9:\"person_id\";s:1:\"1\";s:8:\"cartRecv\";a:0:{}s:9:\"recv_mode\";s:7:\"receive\";s:8:\"supplier\";s:2:\"-1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('763738b2797e0dd39407b1a6d3bf8c97', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464802597, 'a:9:{s:9:\"person_id\";s:1:\"1\";s:4:\"cart\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}s:9:\"sale_mode\";s:4:\"sale\";s:8:\"customer\";s:2:\"44\";s:8:\"payments\";a:0:{}s:9:\"data_save\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}s:8:\"cartRecv\";a:1:{i:1;a:10:{s:7:\"item_id\";s:1:\"7\";s:4:\"line\";i:1;s:4:\"name\";s:7:\"Pantene\";s:11:\"description\";s:71:\"shampo oijdpojsñoifhñhggñohkuhvbknbsfdhvbvsñodvb.skufdvbludfbvisudb\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:5:\"90.00\";}}s:9:\"recv_mode\";s:7:\"receive\";s:8:\"supplier\";s:1:\"9\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('78c5066fadd23b719138ff2e6ed08d2d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1462288177, 'a:10:{s:9:\"person_id\";s:1:\"1\";s:18:\"customer_servicios\";s:1:\"3\";s:4:\"cart\";a:0:{}s:9:\"sale_mode\";s:4:\"sale\";s:8:\"customer\";s:2:\"-1\";s:8:\"payments\";a:0:{}s:9:\"data_save\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}s:8:\"cartRecv\";a:2:{i:1;a:10:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:6:\"220.00\";}i:2;a:10:{s:7:\"item_id\";s:1:\"6\";s:4:\"line\";i:2;s:4:\"name\";s:25:\"gamesa donas de chocolate\";s:11:\"description\";s:110:\"aqui una breve descripcion de lo que es el producto prueba test test aqui una breve descripcion del proidiucto\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:12:\"121211121.00\";}}s:9:\"recv_mode\";s:7:\"receive\";s:8:\"supplier\";s:2:\"10\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('81ba40e96c5f78c8334f26f99d8144df', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1465325690, 'a:1:{s:9:\"person_id\";s:1:\"1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('829bbb69ef09b3e7f7377bbdafc28aef', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1466462662, 'a:3:{s:9:\"person_id\";s:1:\"1\";s:9:\"recv_mode\";s:7:\"receive\";s:18:\"customer_servicios\";s:2:\"-1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('856486a5fd7d150185f8d643696bb5cb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1462377059, 'a:8:{s:9:\"person_id\";s:1:\"1\";s:8:\"cartRecv\";a:0:{}s:9:\"recv_mode\";s:7:\"receive\";s:8:\"supplier\";s:2:\"11\";s:4:\"cart\";a:0:{}s:9:\"sale_mode\";s:4:\"sale\";s:8:\"customer\";s:1:\"3\";s:8:\"payments\";a:0:{}}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('87d438d6a3cc316a501b9e4c17a7b86e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464202895, 'a:1:{s:9:\"person_id\";s:1:\"1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('8881357b84de1f90d763f173e2232a0f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464386206, 'a:4:{s:9:\"person_id\";s:1:\"1\";s:8:\"cartRecv\";a:0:{}s:9:\"recv_mode\";s:7:\"receive\";s:8:\"supplier\";s:2:\"-1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('8bb848affc749a6b3b3194b0d8480df0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1463153102, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('8cb3a28a0f2048268570e2f8f5042c8e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1462304204, 'a:2:{s:9:\"person_id\";s:1:\"1\";s:18:\"customer_servicios\";s:2:\"-1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('8e6c14feaaf2a8e6f1a0fa62fb7646f2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464966465, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('8f9c4891d9d484a1f2483aae67ef1a5b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464796158, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('94a9127bfcfaec2e1b8e400cd3262136', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1462378358, 'a:6:{s:9:\"person_id\";s:1:\"1\";s:4:\"cart\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"6\";s:4:\"line\";i:1;s:4:\"name\";s:25:\"gamesa donas de chocolate\";s:11:\"item_number\";s:12:\"232133221122\";s:11:\"description\";s:110:\"aqui una breve descripcion de lo que es el producto prueba test test aqui una breve descripcion del proidiucto\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}s:9:\"sale_mode\";s:4:\"sale\";s:8:\"customer\";s:1:\"5\";s:8:\"payments\";a:0:{}s:9:\"data_save\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"6\";s:4:\"line\";i:1;s:4:\"name\";s:25:\"gamesa donas de chocolate\";s:11:\"item_number\";s:12:\"232133221122\";s:11:\"description\";s:110:\"aqui una breve descripcion de lo que es el producto prueba test test aqui una breve descripcion del proidiucto\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('9a1b0962680ec316c0610bfdb24ea2ed', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1462373844, 'a:7:{s:9:\"person_id\";s:1:\"1\";s:18:\"customer_servicios\";s:2:\"-1\";s:4:\"cart\";a:2:{i:1;a:11:{s:7:\"item_id\";s:1:\"6\";s:4:\"line\";i:1;s:4:\"name\";s:25:\"gamesa donas de chocolate\";s:11:\"item_number\";s:12:\"232133221122\";s:11:\"description\";s:110:\"aqui una breve descripcion de lo que es el producto prueba test test aqui una breve descripcion del proidiucto\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";s:1:\"5\";s:8:\"discount\";s:1:\"0\";s:5:\"price\";s:4:\"1.00\";}i:2;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:2;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";s:1:\"5\";s:8:\"discount\";s:1:\"0\";s:5:\"price\";s:4:\"1.00\";}}s:9:\"sale_mode\";s:4:\"sale\";s:8:\"customer\";s:1:\"5\";s:8:\"payments\";a:0:{}s:9:\"data_save\";a:2:{i:1;a:11:{s:7:\"item_id\";s:1:\"6\";s:4:\"line\";i:1;s:4:\"name\";s:25:\"gamesa donas de chocolate\";s:11:\"item_number\";s:12:\"232133221122\";s:11:\"description\";s:110:\"aqui una breve descripcion de lo que es el producto prueba test test aqui una breve descripcion del proidiucto\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}i:2;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:2;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('9f0f989a3be64022013b4ce16973b1ec', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1462378723, 'a:11:{s:9:\"person_id\";s:1:\"1\";s:4:\"kits\";a:0:{}s:8:\"cartRecv\";a:0:{}s:9:\"recv_mode\";s:7:\"receive\";s:8:\"supplier\";s:2:\"-1\";s:4:\"cart\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}s:9:\"sale_mode\";s:4:\"sale\";s:8:\"customer\";s:2:\"-1\";s:8:\"payments\";a:0:{}s:9:\"data_save\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}s:18:\"customer_servicios\";s:2:\"-1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('a9da74e952c5799f67223822fe25da58', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464972165, 'a:10:{s:9:\"person_id\";s:1:\"1\";s:8:\"cartRecv\";a:1:{i:1;a:10:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:6:\"220.00\";}}s:9:\"recv_mode\";s:7:\"receive\";s:8:\"supplier\";s:2:\"-1\";s:18:\"customer_servicios\";s:2:\"-1\";s:4:\"cart\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}s:9:\"sale_mode\";s:4:\"sale\";s:8:\"customer\";s:2:\"-1\";s:8:\"payments\";a:0:{}s:9:\"data_save\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('aa756aa3e8dac724d7bd41a0a81a589c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Geck', 1468535569, 'a:2:{s:9:\"person_id\";s:1:\"1\";s:18:\"customer_servicios\";s:2:\"-1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('ac960f35da686ee191eb527090cd9328', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464201692, 'a:2:{s:9:\"person_id\";s:1:\"1\";s:18:\"customer_servicios\";s:2:\"-1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('ae283ccaf71195bd7eafddcd0d146e85', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1468452447, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('aeb55fef2ef56315b332822aa246a989', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464796153, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('b71226ac8b2b09481db7200e70e30a4b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464808106, 'a:6:{s:9:\"person_id\";s:1:\"1\";s:4:\"cart\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}s:9:\"sale_mode\";s:4:\"sale\";s:8:\"customer\";s:2:\"-1\";s:8:\"payments\";a:0:{}s:9:\"data_save\";a:1:{i:1;a:11:{s:7:\"item_id\";s:1:\"4\";s:4:\"line\";i:1;s:4:\"name\";s:5:\"arena\";s:11:\"item_number\";s:6:\"124212\";s:11:\"description\";s:70:\"lkjskjdfñlidjgñoihfogjdhnkvihndfvbjfb lkdj vblkjfdvbljfdblkjfdvlibvj\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:4:\"1.00\";}}}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('b94d854bde0bd0a22f74096628669707', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464796156, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('ba6f3306bee95639b56e31d439e1d15a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1467996719, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('bb54043f00a6bf41f7bb0cad2bb765a8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464795703, 'a:1:{s:9:\"person_id\";s:1:\"1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('c72ed084e5474b876bf608046ae2ef21', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1465401715, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('ce9fda2f7383c8f17aeea58174e8e84d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1462299163, 'a:4:{s:9:\"person_id\";s:1:\"1\";s:8:\"cartRecv\";a:0:{}s:9:\"recv_mode\";s:7:\"receive\";s:8:\"supplier\";s:2:\"-1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('cf459231345ba4d18e460ae8757eda0f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464711392, 'a:1:{s:9:\"person_id\";s:1:\"1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('dc0fc9dd320a09b283cee0ede2686447', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1464796154, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('dc2b404786356417dc7503f1a42dcc53', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1467996725, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('e5c03e7745f039c5d1cdfd2bd00e1791', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1467996726, NULL);
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('f18607bad780dade86aec5a6a4004b3c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1465325991, 'a:2:{s:9:\"person_id\";s:1:\"1\";s:18:\"customer_servicios\";s:2:\"-1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('f355f5b6878cd7e05010b5d68641d6a7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1465410487, 'a:1:{s:9:\"person_id\";s:1:\"1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('f4bcc8e7a8fe98ee0d7e27868225c0ab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1467996728, 'a:2:{s:9:\"person_id\";s:1:\"1\";s:18:\"customer_servicios\";s:2:\"48\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('f4d9f95c4003f693487369ba4b49bb22', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1462298609, 'a:4:{s:9:\"person_id\";s:1:\"1\";s:8:\"cartRecv\";a:1:{i:1;a:10:{s:7:\"item_id\";s:1:\"5\";s:4:\"line\";i:1;s:4:\"name\";s:48:\"arena mexico-relkmnkjjkjoljfjdpoifjpojkdfpodfdpf\";s:11:\"description\";s:85:\"dasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasas\";s:12:\"serialnumber\";s:0:\"\";s:21:\"allow_alt_description\";s:1:\"0\";s:13:\"is_serialized\";s:1:\"0\";s:8:\"quantity\";i:1;s:8:\"discount\";i:0;s:5:\"price\";s:8:\"23212.00\";}}s:9:\"recv_mode\";s:7:\"receive\";s:8:\"supplier\";s:1:\"4\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('f575aca642e82919f3518046538b3b72', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1467734024, 'a:2:{s:9:\"person_id\";s:1:\"1\";s:18:\"customer_servicios\";s:2:\"-1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('f855d1652cdf88523a2e1b4ac5a03ed6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1468543427, 'a:1:{s:9:\"person_id\";s:1:\"1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('fc4a8572af9991bf55c8994c346d8115', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1467254497, 'a:2:{s:9:\"person_id\";s:1:\"1\";s:18:\"customer_servicios\";s:2:\"-1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('fc7bb789c23891c199ba7f0dcb0adab5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1463517363, 'a:1:{s:9:\"person_id\";s:1:\"1\";}');
INSERT INTO ospos_sessions (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES ('fcb730e1fef64406d3d64cd12ff1e402', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1467992387, 'a:2:{s:9:\"person_id\";s:1:\"1\";s:18:\"customer_servicios\";s:2:\"-1\";}');


#
# TABLE STRUCTURE FOR: ospos_suppliers
#

DROP TABLE IF EXISTS ospos_suppliers;

CREATE TABLE `ospos_suppliers` (
  `person_id` int(10) NOT NULL,
  `company` varchar(255) NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `account_number` (`account_number`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `ospos_suppliers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO ospos_suppliers (`person_id`, `company`, `account_number`, `deleted`) VALUES (4, 'el chato', NULL, 1);
INSERT INTO ospos_suppliers (`person_id`, `company`, `account_number`, `deleted`) VALUES (6, 'el chato', NULL, 0);
INSERT INTO ospos_suppliers (`person_id`, `company`, `account_number`, `deleted`) VALUES (7, 'grupo reconocimiento de la palma de las peñas', NULL, 0);
INSERT INTO ospos_suppliers (`person_id`, `company`, `account_number`, `deleted`) VALUES (8, 'grupo reconocimiento de la palma de las peñas', NULL, 0);
INSERT INTO ospos_suppliers (`person_id`, `company`, `account_number`, `deleted`) VALUES (9, 'grupo reconocimiento de la palma de las peñas, reconocimiento de la palma de las peña', NULL, 0);
INSERT INTO ospos_suppliers (`person_id`, `company`, `account_number`, `deleted`) VALUES (10, 'arena', NULL, 0);
INSERT INTO ospos_suppliers (`person_id`, `company`, `account_number`, `deleted`) VALUES (11, 'arena', NULL, 0);
INSERT INTO ospos_suppliers (`person_id`, `company`, `account_number`, `deleted`) VALUES (45, 'gamesa', NULL, 0);


